import 'package:libmolk_dart/cereal/json.dart';
import 'package:libmolk_dart/cereal/lib.dart';
import 'package:test/expect.dart';
import 'package:test/scaffolding.dart';

void main() {
  test("emitter", () {
    final object = StrongCerealObject.from({
      'name': CerealString('Molkars'),
      'email': CerealString('<email>'),
      'age': CerealNone(),
    });

    final manager = CerealManager();
    final emitter = JsonEmitter(manager, 2);
    final result = emitter.emit(object);

    print(result);

    expect(result.isOk, equals(true));

    final string = result.unwrap();
    print(string);

    const output = """{
  "name": "Molkars",
  "email": "<email>",
  "age": null
}""";

    expect(string, equals(output));
  });
}
