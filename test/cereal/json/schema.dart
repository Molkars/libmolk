import 'package:libmolk_dart/cereal/lib.dart';
import 'package:test/scaffolding.dart';

void main() {
  test('schema', () {
    final _schema = const StrongObjectSchema({
      "id": IntSchema(),
      "name": StringSchema(),
      "email": StringSchema(),
      "created_at": DateTimeSchema(),
    });

    final value = BasicCerealObject({
      "id": CerealInt(1),
      "name": CerealString("hi"),
      "email": CerealString("there"),
      "created_at": CerealString(DateTime(2020).toIso8601String()),
    });

    final out = _schema.form(value).unwrap();
    print(out.value);

    final formatter = CerealPrimitiveFormatter();
    print(formatter.format(out));
  });
}