
import 'package:libmolk_dart/cereal/json.dart';
import 'package:libmolk_dart/cereal/lib.dart';
import 'package:test/test.dart';

void main() {
  test("test parse", () {
    final json = '''
{
  "string": "test_string",
  "int": -0,
  "double": 0.0,
  "double-exp": -103.43e-3,
  "list": [-12, 34.0e3, "str", [1, 2, 3], {"a":"b","c":"d"}, true, false, null, {}]
}''';

    final parser = JsonParser(json.codeUnits);
    final result = parser.parse().unwrap();

    final formatter = CerealPrimitiveFormatter();
    print(formatter.visitCerealValue(result));

  });
}