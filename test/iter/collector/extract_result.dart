

import 'package:libmolk_dart/base/result.dart';
import 'package:libmolk_dart/libmolk_dart.dart';

class ExtractResultCollector<T, E, O> with Collector<Result<T, E>, Result<O, E>> {
  final Collector<T, O> _inner;

  ExtractResultCollector(this._inner);

  @override
  Result<O, E> collect(Iter<Result<T, E>> iter) {
    var inner = MVector<T>();
    for (var elem in iter.iterable()) {
      if (elem.isErr) {
        return elem as Result<O, E>;
      }
      inner.push(elem.unwrap());
    }

    return Result.ok(_inner.collect(inner.iter()));
  }
}