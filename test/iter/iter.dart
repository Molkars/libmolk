import 'package:libmolk_dart/iter/iter.dart';
import 'package:libmolk_dart/list/linked_list.dart';
import 'package:test/test.dart';

void main() {
  final list = MLinkedList.from([1, 2, 3, 4, 5]);
  test('Dart Iterator', () {
    final iter = list.iter();
    final dartIter = iter.intoDartIter();
    expect(dartIter.moveNext(), isTrue);
    expect(dartIter.current, 1);
    expect(dartIter.moveNext(), isTrue);
    expect(dartIter.current, 2);
    expect(dartIter.moveNext(), isTrue);
    expect(dartIter.current, 3);
    expect(dartIter.moveNext(), isTrue);
    expect(dartIter.current, 4);
    expect(dartIter.moveNext(), isTrue);
    expect(dartIter.current, 5);
    expect(dartIter.moveNext(), isFalse);
  });

  test('For-Each', () {
    var count = 0;
    list.iter().forEach((v) => count += v).ignore();
    expect(count, 15);
  });

  test('Map', () {
    final iter = list.iter();
    final mapped = iter.map((v) => v * 2);
    expect(mapped.next(), Option.some(2));
    expect(mapped.next(), Option.some(4));
    expect(mapped.next(), Option.some(6));
    expect(mapped.next(), Option.some(8));
    expect(mapped.next(), Option.some(10));
    expect(mapped.next(), Option<int>.none());
  });

  test('Filter', () {
    final iter = list.iter();
    final filtered = iter.filter((v) => v % 2 == 0);
    expect(filtered.next(), Option.some(2));
    expect(filtered.next(), Option.some(4));
    expect(filtered.next(), Option.none());
  });

  test('Fold', () {
    final iter = list.iter();
    final prod = iter.fold<int>(1, (acc, v) => acc * v);
    expect(prod, 120);
  });

  test('Collect to List', () {
    final iter = list.iter();
    final collected = iter.collect(ListCollector());
    expect(collected, [1, 2, 3, 4, 5]);
  });

  test('Position', () {
    final iter = list.iter();
    expect(iter.position((v) => v == 3), equals(Option.some(2)));
  });
}
