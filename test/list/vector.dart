
import 'package:libmolk_dart/list/vector.dart';
import 'package:test/test.dart';

import 'util.dart';

void main() {
  test('Push', () => testPush(MVector.new));

  test('Remove', () => testRemove(MVector.new));
}