import 'package:libmolk_dart/base/option.dart';
import 'package:libmolk_dart/list/linked_list.dart';
import 'package:test/test.dart';

void main() {
  test("Linked List Append", () {
    final list = MLinkedList<int>();
    list.append(1);
    expect(list.length(), 1);

    list.append(2);
    expect(list.length(), 2);

    list.append(3);
    expect(list.length(), 3);

    list.append(0);
    expect(list.length(), 4);

    expect(list.popFront(), Option.some(1));
    expect(list.length(), 3);

    expect(list.popFront(), Option.some(2));
    expect(list.length(), 2);

    expect(list.popFront(), Option.some(3));
    expect(list.length(), 1);

    expect(list.popFront(), Option.some(0));
    expect(list.length(), 0);

    expect(list.popFront(), Option.none());
    expect(list.length(), 0);
  });

  test("Linked List Prepend", () {
    final list = MLinkedList<int>();

    list.prepend(1);
    expect(list.length(), 1);

    list.prepend(2);
    expect(list.length(), 2);

    list.prepend(3);
    expect(list.length(), 3);

    list.prepend(0);
    expect(list.length(), 4);

    expect(list.popBack(), Option.some(1));
    expect(list.length(), 3);

    expect(list.popBack(), Option.some(2));
    expect(list.length(), 2);

    expect(list.popBack(), Option.some(3));
    expect(list.length(), 1);

    expect(list.popBack(), Option.some(0));
    expect(list.length(), 0);

    expect(list.popBack(), Option.none());
    expect(list.length(), 0);
  });

  test("Forward Iter", () {
    final list = MLinkedList<int>();
    list.append(1);
    list.append(2);
    list.append(3);
    final iter = list.iter();
    expect(iter.next(), Option.some(1));
    expect(iter.next(), Option.some(2));
    expect(iter.next(), Option.some(3));
    expect(iter.next(), Option.none());
  });

  test("Reverse Iter", () {
    final list = MLinkedList<int>();
    list.append(1);
    list.append(2);
    list.append(3);
    final iter = list.revIter();
    expect(iter.next(), Option.some(3));
    expect(iter.next(), Option.some(2));
    expect(iter.next(), Option.some(1));
    expect(iter.next(), Option.none());
  });
}
