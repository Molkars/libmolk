
import 'dart:math';

import 'package:libmolk_dart/base/option.dart';
import 'package:libmolk_dart/list/list.dart';
import 'package:test/test.dart';

void testPush(MList<int> Function() factory) {
  final list = factory();
  expect(list.length(), isZero);

  list.push(1);
  expect(list.length(), equals(1));

  list.push(2);
  expect(list.length(), equals(2));

  list.push(3);
  expect(list.length(), equals(3));

  final iter = list.iter();
  expect(iter.next(), equals(Option.some(1)));
  expect(iter.next(), equals(Option.some(2)));
  expect(iter.next(), equals(Option.some(3)));
  expect(iter.next(), equals(Option.none()));
}

void testRemove(MList<int> Function() factory) {
  final list = factory();
  expect(list.length(), isZero);

  final random = Random(0xF00);
  final start = random.nextInt(100);
  final values = List.generate(random.nextInt(100), (i) => start + i);
  values.shuffle(random);

  for (var length = 0; length < values.length; length++) {
    list.push(values[length]);
    expect(list.length(), length + 1);
  }

  values.shuffle(random);
  for (var length = 0; length < values.length; length++) {
    expect(list.remove(values[length]), isTrue);
    expect(list.length(), values.length - length - 1);
  }
}