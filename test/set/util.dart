
import 'dart:math';

import 'package:libmolk_dart/set/set.dart';
import 'package:test/test.dart';

void testFrom<T extends MSet<int>>(T Function(List<int> keys) factory) {
  final keys = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  final set = factory(keys);

  expect(set.count(), keys.length);
  for (var key in keys) {
    expect(set.contains(key), isTrue);
  }
}

void testInsert<T extends MSet<int>>(T Function() factory) {
  final set = factory();
  expect(set.count(), isZero);

  expect(set.insert(1), isTrue);
  expect(set.insert(1), isFalse);
  expect(set.count(), equals(1));

  expect(set.insert(2), isTrue);
  expect(set.insert(2), isFalse);
  expect(set.count(), equals(2));

  expect(set.insert(1), isFalse);
  expect(set.count(), equals(2));
}

void testContains<T extends MSet<int>>(T Function() factory) {
  final set = factory();
  expect(set.count(), isZero);

  expect(set.contains(1), isFalse);
  expect(set.insert(1), isTrue);
  expect(set.contains(1), isTrue);

  expect(set.contains(2), isFalse);
  expect(set.insert(2), isTrue);
  expect(set.contains(2), isTrue);

  expect(set.contains(3), isFalse);
  expect(set.insert(3), isTrue);
  expect(set.contains(3), isTrue);

  expect(set.contains(1), isTrue);
}

void testRemove<T extends MSet<int>>(T Function() factory) {
  for (var seed = 0; seed < 10000; seed++) {
    final set = factory();
    expect(set.count(), equals(0));

    final random = Random(seed);
    final keys = List<int>.generate(random.nextInt(100), (i) => i);

    keys.shuffle(random);
    for (var key in keys) {
      set.insert(key);
    }

    keys.shuffle(random);
    for (var key in keys) {
      expect(set.remove(key), isTrue);
      expect(set.contains(key), isFalse);
    }
  }
}