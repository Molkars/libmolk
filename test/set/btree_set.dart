import 'package:libmolk_dart/base/option.dart';
import 'package:libmolk_dart/set/btree_set.dart';
import 'package:test/test.dart';

import 'util.dart';

void main() {
  test('Insertion', () => testInsert<MBTreeSet<int>>(MBTreeSet.new));

  test('.From', () => testFrom<MBTreeSet<int>>(MBTreeSet.from));

  test('Contains', () => testContains<MBTreeSet<int>>(MBTreeSet.new));

  test('Remove', () => testRemove<MBTreeSet<int>>(MBTreeSet.new));

  test('Traversal Iterators', () {
    final set = MBTreeSet.from([8, 4, 12, 2, 6, 10, 14]);

    final inOrder = set.inOrder();
    expect(inOrder.next(), equals(Option.some(2)));
    expect(inOrder.next(), equals(Option.some(4)));
    expect(inOrder.next(), equals(Option.some(6)));
    expect(inOrder.next(), equals(Option.some(8)));
    expect(inOrder.next(), equals(Option.some(10)));
    expect(inOrder.next(), equals(Option.some(12)));
    expect(inOrder.next(), equals(Option.some(14)));
    expect(inOrder.next(), equals(Option.none()));

    final preOrder = set.preOrder();
    expect(preOrder.next(), equals(Option.some(8)));
    expect(preOrder.next(), equals(Option.some(4)));
    expect(preOrder.next(), equals(Option.some(2)));
    expect(preOrder.next(), equals(Option.some(6)));
    expect(preOrder.next(), equals(Option.some(12)));
    expect(preOrder.next(), equals(Option.some(10)));
    expect(preOrder.next(), equals(Option.some(14)));
    expect(preOrder.next(), equals(Option.none()));

    final postOrder = set.postOrder();
    expect(postOrder.next(), equals(Option.some(2)));
    expect(postOrder.next(), equals(Option.some(6)));
    expect(postOrder.next(), equals(Option.some(4)));
    expect(postOrder.next(), equals(Option.some(10)));
    expect(postOrder.next(), equals(Option.some(14)));
    expect(postOrder.next(), equals(Option.some(12)));
    expect(postOrder.next(), equals(Option.some(8)));
    expect(postOrder.next(), equals(Option.none()));
  });
}
