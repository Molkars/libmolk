import 'package:libmolk_dart/set/hash_set.dart';
import 'package:test/scaffolding.dart';

import 'util.dart';

void main() {
  test('Insertion', () => testInsert<MHashSet<int>>(MHashSet.new));

  test('.From', () => testFrom<MHashSet<int>>(MHashSet.from));

  test('Contains', () => testContains<MHashSet<int>>(MHashSet.new));

  test('Remove', () => testRemove<MHashSet<int>>(MHashSet.new));
}