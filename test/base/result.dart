
import 'package:libmolk_dart/base/result.dart';
import 'package:test/expect.dart';
import 'package:test/scaffolding.dart';

void main() {
  group('Result Tests', () {
    test('ok', () {
      final result = Result.ok(1);
      expect(result.isOk && !result.isErr && result.unwrap() == 1, isTrue);
    });

    test('err', () {
      final result = Result.err(1);
      expect(!result.isOk && result.isErr && result.unwrapErr() == 1, isTrue);
    });

    
  });
}