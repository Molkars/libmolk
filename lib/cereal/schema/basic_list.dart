part of 'lib.dart';

class BasicListSchema with CerealSchema {
  final List<CerealSchema> value;

  const BasicListSchema(this.value);

  @override
  Result<BasicCerealList, CerealFormationError> form(CerealPrimitive value) {
    return value.cerealMap(
      list: (val) => Result.ok(val),
      orElse: (val) => Result.err(CerealFormationError.expectedType(BasicCerealList, val.concreteType)),
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BasicListSchema && runtimeType == other.runtimeType && _listEq(value, other.value) ||
      other is StrongListSchema && value.every((element) => element == other.type);

  @override
  int get hashCode => value.hashCode;
}
