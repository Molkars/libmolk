part of 'lib.dart';

class OptionSchema with CerealSchema {
  final Option<CerealSchema> elementType;

  const OptionSchema(this.elementType);

  @override
  Result<CerealOption, CerealFormationError> form(CerealPrimitive value) {
    return Result.ok(value.cerealMap(
      none: CerealOption.none,
      orElse: CerealOption.new,
    ));
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is OptionSchema && runtimeType == other.runtimeType && elementType == other.elementType;

  @override
  int get hashCode => elementType.hashCode;
}