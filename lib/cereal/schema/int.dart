part of 'lib.dart';

class IntSchema with CerealSchema {
  const IntSchema();

  @override
  Result<CerealInt, CerealFormationError> form(CerealPrimitive value) {
    return value.cerealMap(
      int: Result.ok,
      orElse: (val) => Result.err(CerealFormationError.expectedType(CerealInt, val.concreteType)),
    );
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is IntSchema && runtimeType == other.runtimeType;

  @override
  int get hashCode => 0;
}