part of libmolk.cereal.schema;

class DoubleSchema with CerealSchema {
  const DoubleSchema();

  @override
  Result<CerealDouble, CerealFormationError> form(CerealPrimitive value) {
    return value.cerealMap(
      double: Result.ok,
      orElse: (val) => Result.err(CerealFormationError.expectedType(CerealDouble, val.concreteType)),
    );
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is DoubleSchema && runtimeType == other.runtimeType;

  @override
  int get hashCode => 0;
}

