part of 'lib.dart';

class WeakObjectSchema with CerealSchema {
  const WeakObjectSchema();

  @override
  Result<CerealValue, CerealFormationError> form(CerealPrimitive value) {
    return value.cerealMap(
      object: (val) => Result.ok(WeakCerealObject(Map.of(val.items))),
      orElse: (val) => Result.err(CerealFormationError.expectedType(CerealValue, val.concreteType)),
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is WeakObjectSchema && runtimeType == other.runtimeType;

  @override
  int get hashCode => 0;
}
