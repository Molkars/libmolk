part of 'lib.dart';

class StrongObjectSchema with CerealSchema {
  final Map<String, CerealSchema> fields;

  static const StrongObjectSchema empty = StrongObjectSchema({});

  const StrongObjectSchema(this.fields);

  @override
  Result<StrongCerealObject, CerealFormationError> form(CerealPrimitive value) {
    return value.cerealMap(
      object: (value) => Result.block((tri) {
        final _keys = fields.keys.toSet();
        final _diff = Set.of(_keys);

        final out = <String, CerealValue>{};
        for (final pair in value.items.entries) {
          if (!_keys.contains(pair.key)) {
            return Result.err(CerealFormationError("[StrongObject] Unexpected key ${pair.key}"));
          }
          if (out.containsKey(pair.key)) {
            return Result.err(CerealFormationError("[StrongObject] Duplicate key ${pair.key}"));
          }
          final type = fields[pair.key]!;
          final value = type.form(pair.value)(tri);
          out[pair.key] = value;
          _diff.remove(pair.key);
        }

        if (_diff.isNotEmpty) {
          return Result.err(CerealFormationError("[StrongObject] Missing keys: ${_diff.join(", ")}"));
        } else {
          return Result.ok(StrongCerealObject.from(out));
        }
      }),
      orElse: (val) => Result.err(CerealFormationError.expectedType(BasicCerealObject, val.concreteType)),
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is StrongObjectSchema && runtimeType == other.runtimeType && _mapEq(fields, other.fields);

  @override
  int get hashCode => fields.hashCode;
}
