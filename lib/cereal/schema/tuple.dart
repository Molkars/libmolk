part of 'lib.dart';

class TupleSchema with CerealSchema {
  List<CerealSchema> _types;

  TupleSchema(this._types);

  UnmodifiableListView<CerealSchema> get types => UnmodifiableListView(_types);

  @override
  Result<CerealTuple, CerealFormationError> form(CerealPrimitive value) {
    return value.cerealMap(
      list: (val) => Result.block((tri) {
        if (val.items.length != _types.length) {
          return Result.err(
              CerealFormationError("Tuple formation error: number of values did not match number of types"));
        }

        final values = val.items.iter();
        final types = _types.iter();

        final out = <CerealValue>[];
        for (final pair in values.zip(types).iterable()) {
          final primValue = pair.left;
          final type = pair.right;

          final value = type.form(primValue).tri(tri);
          out.add(value);
        }

        return Result.ok(CerealTuple(out));
      }),
      orElse: (val) => Result.err(CerealFormationError.expectedType(BasicCerealList, val.concreteType)),
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TupleSchema && runtimeType == other.runtimeType && _listEq(_types, other._types);

  @override
  int get hashCode => _types.hashCode;
}
