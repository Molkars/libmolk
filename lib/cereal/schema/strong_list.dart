part of 'lib.dart';

class StrongListSchema with CerealSchema, CerealListSchema {
  final CerealSchema type;

  const StrongListSchema(this.type);

  @override
  Result<StrongCerealList, CerealFormationError> form(CerealPrimitive value) {
    return value.cerealMap(
      list: (val) => Result.block((tri) {
        final out = <CerealValue>[];
        for (final item in val.items) {
          final value = type.form(item)(tri);
          out.add(value);
        }
        return Result.ok(StrongCerealList.from(out));
      }),
      orElse: (val) => Result.err(CerealFormationError.expectedType(BasicCerealList, val.concreteType)),
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is StrongListSchema && runtimeType == other.runtimeType && type == other.type ||
      other is WeakListSchema && other.length == 0 ||
      other is BasicListSchema && other.value.every((element) => element == type);

  @override
  int get hashCode => type.hashCode;

  @override
  bool match(CerealSchema schema, int index) => schema == type;
}
