part of libmolk.cereal.schema;

class StringSchema with CerealSchema {
  const StringSchema();

  @override
  Result<CerealString, CerealFormationError> form(CerealPrimitive value) {
    return value.cerealMap(
      string: Result.ok,
      orElse: (val) => Result.err(CerealFormationError.expectedType(CerealPrimitive, val.concreteType)),
    );
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is StringSchema && runtimeType == other.runtimeType;

  @override
  int get hashCode => 0;
}

