part of 'lib.dart';

class ArraySchema with CerealSchema {
  final Option<CerealSchema> schema;
  final int length;

  const ArraySchema.raw(this.schema, this.length);

  const ArraySchema.unknown()
      : schema = const Option<CerealSchema>.none(),
        length = 0;

  @override
  Result<CerealValue, CerealFormationError> form(CerealPrimitive value) {
    return value.cerealMap(
      list: (val) => Result.block((tri) {
        final out = <CerealValue>[];
        for (final elem in val.items) {
          final type = schema.expect("array schema: issue");
          final value = type.form(elem).tri(tri);
          out.add(value);
        }
        return Result.ok(CerealArray(out));
      }),
      orElse: (val) => Result.err(CerealFormationError.expectedType(BasicCerealList, val.concreteType)),
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ArraySchema && runtimeType == other.runtimeType && schema == other.schema && length == other.length;

  @override
  int get hashCode => schema.hashCode ^ length.hashCode;
}
