part of libmolk.cereal.schema;

class BoolSchema with CerealSchema {
  const BoolSchema();

  @override
  Result<CerealBool, CerealFormationError> form(CerealPrimitive value) {
    return value.cerealMap(
      bool: Result.ok,
      orElse: (val) => Result.err(CerealFormationError.expectedType(CerealBool, val.concreteType)),
    );
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is BoolSchema && runtimeType == other.runtimeType;

  @override
  int get hashCode => 0;
}
