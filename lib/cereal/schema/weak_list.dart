part of 'lib.dart';

class WeakListSchema with CerealSchema, CerealListSchema {
  final int length;

  const WeakListSchema(this.length);

  @override
  Result<WeakCerealList, CerealFormationError> form(CerealPrimitive value) {
    return value.cerealMap(
      list: (val) => Result.ok(WeakCerealList(List.of(val.items))),
      orElse: (val) => Result.err(CerealFormationError.expectedType(BasicCerealList, val.concreteType)),
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is WeakListSchema && runtimeType == other.runtimeType && length == other.length ||
      other is BasicListSchema && other.value.length == length;

  @override
  int get hashCode => length.hashCode;

  @override
  bool match(CerealSchema schema, int index) => true;
}
