library libmolk.cereal.schema;

import 'package:libmolk_dart/cereal/lib.dart';
import 'package:libmolk_dart/libmolk_dart.dart';
import 'package:meta/meta.dart';

part 'array.dart';
part 'basic_list.dart';
part 'basic_object.dart';
part 'bool.dart';
part 'date_time.dart';
part 'double.dart';
part 'int.dart';
part 'none.dart';
part 'option.dart';
part 'string.dart';
part 'strong_list.dart';
part 'strong_object.dart';
part 'tuple.dart';
part 'weak_list.dart';
part 'weak_object.dart';

@immutable
mixin CerealSchema {
  Result<CerealValue, CerealFormationError> form(CerealPrimitive value);
}

mixin CerealListSchema on CerealSchema {
  bool match(CerealSchema schema, int index);
}

class CerealFormationError {
  final String message;

  const CerealFormationError(this.message);

  const CerealFormationError.expectedType(Type expected, Type actual)
      : this("Expected type `$expected`, instead found `$actual`");

  @override
  String toString() {
    return 'CerealFormationError{message: $message}';
  }
}

bool _mapEq<K, V>(Map<K, V> a, Map<K, V> b) => a.keys.toSet() == b.keys.toSet() && a.keys.every((k) => a[k]! == b[k]!);
bool _listEq<E>(List<E> a, List<E> b) => a.length == b.length && a.iter().zip(b.iter()).every((a, b) => a == b);
