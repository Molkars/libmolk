part of 'lib.dart';

class BasicObjectSchema with CerealSchema {
  final Map<String, CerealSchema> _entries;

  UnmodifiableMapView<String, CerealSchema> get entries => UnmodifiableMapView(_entries);

  const BasicObjectSchema(this._entries);

  @override
  Result<BasicCerealObject, CerealFormationError> form(CerealPrimitive value) {
    return value.cerealMap(
      object: (val) => Result.ok(val),
      orElse: (val) => Result.err(CerealFormationError.expectedType(BasicCerealObject, val.concreteType)),
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BasicObjectSchema && runtimeType == other.runtimeType && _mapEq(_entries, other._entries) ||
      other is StrongObjectSchema && _mapEq(other.fields, entries);

  @override
  int get hashCode => _entries.hashCode;
}
