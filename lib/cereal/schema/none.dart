part of 'lib.dart';

class NoneSchema with CerealSchema {
  const NoneSchema();

  @override
  Result<CerealNone, CerealFormationError> form(CerealPrimitive value) {
    return value.cerealMap(
      none: () => const Result.ok(CerealNone()),
      orElse: (val) => Result.err(CerealFormationError.expectedType(CerealNone, val.concreteType)),
    );
  }

  @override
  bool operator ==(Object other) => identical(this, other) || other is NoneSchema && runtimeType == other.runtimeType;

  @override
  int get hashCode => 0;
}