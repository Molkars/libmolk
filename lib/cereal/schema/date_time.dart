part of 'lib.dart';

class DateTimeSchema with CerealSchema {
  const DateTimeSchema();

  @override
  Result<CerealValue, CerealFormationError> form(CerealPrimitive value) {
    return value.cerealMap(
      string: (value) =>
          Result.wrap<DateTime, FormatException>(() => DateTime.parse(value.value))
              .map(CerealDateTime.new)
              .mapErr((err) => CerealFormationError("Error [DateTimeSchema]: $err")),
      orElse: (val) => Result.err(CerealFormationError.expectedType(CerealString, val.runtimeType)),
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is DateTimeSchema && runtimeType == other.runtimeType;

  @override
  int get hashCode => 0;
}