import 'dart:collection';

import 'package:libmolk_dart/cereal/primitive/lib.dart';
import 'package:libmolk_dart/cereal/value/lib.dart';
import 'package:libmolk_dart/iter/iter.dart';
import 'package:libmolk_dart/iter/pair_iter.dart';

export 'dart:collection' show UnmodifiableListView, UnmodifiableMapView;

export 'package:libmolk_dart/cereal/primitive/lib.dart';
export 'package:libmolk_dart/cereal/schema/lib.dart';
export 'package:libmolk_dart/cereal/value/lib.dart';
export 'package:libmolk_dart/cereal/mill/lib.dart';

part 'printer.dart';

mixin CerealVisitor<T> {
  T defaultVisit();

  T visitCerealValue(CerealValue value) {
    if (value is CerealInt) return visitCerealInt(value.value);
    if (value is CerealString) return visitCerealString(value.value);
    if (value is CerealDouble) return visitCerealDouble(value.value);
    if (value is CerealBool) return visitCerealBool(value.value);
    if (value is CerealNone) return visitCerealNull();
    if (value is CerealDateTime) return visitCerealDateTime(value.value);
    if (value is StrongCerealObject) return visitStrongCerealObject(value.value);
    if (value is StrongCerealList) return visitStrongCerealList(value.value);
    if (value is BasicCerealObject) return visitCerealObject(value.items);
    if (value is BasicCerealList) return visitCerealList(value.items);
    if (value is CerealArray) return visitCerealArray(value.value);
    if (value is CerealTuple) return visitCerealTuple(value.value);
    if (value is WeakCerealList) todo("deprecate weak types");
    if (value is WeakCerealObject) todo("deprecate weak types");
    print(value.runtimeType);
    throw unreachable("Should not be able to read ${value.runtimeType} in $runtimeType#visitCerealValue");
  }

  T visitCerealInt(int value) => defaultVisit();

  T visitCerealDouble(double value) => defaultVisit();

  T visitCerealString(String value) => defaultVisit();

  T visitCerealBool(bool value) => defaultVisit();

  T visitCerealList(UnmodifiableListView<CerealPrimitive> items) => defaultVisit();

  T visitCerealObject(UnmodifiableMapView<String, CerealPrimitive> items) => defaultVisit();

  T visitStrongCerealObject(UnmodifiableMapView<String, CerealValue> items) => defaultVisit();

  T visitStrongCerealList(UnmodifiableListView<CerealValue> items) => defaultVisit();

  T visitCerealDateTime(DateTime dateTime) => defaultVisit();

  T visitCerealNull() => defaultVisit();

  T visitCerealArray(UnmodifiableListView<CerealValue> items) => defaultVisit();

  T visitCerealTuple(UnmodifiableListView<CerealValue> items) => defaultVisit();
}

mixin CerealPrimitiveVisitor<T> {
  T visitPrimitiveInt(int value);
  T visitPrimitiveDouble(double value);
  T visitPrimitiveString(String value);
  T visitPrimitiveBool(bool value);
  T visitPrimitiveNone();
  T visitPrimitiveList(UnmodifiableListView<CerealPrimitive> items);
  T visitPrimitiveObject(UnmodifiableMapView<String, CerealPrimitive> items);

  T visitCerealPrimitive(CerealPrimitive value) {
    if (value is CerealInt) return visitPrimitiveInt(value.value);
    if (value is CerealDouble) return visitPrimitiveDouble(value.value);
    if (value is CerealString) return visitPrimitiveString(value.value);
    if (value is CerealBool) return visitPrimitiveBool(value.value);
    if (value is CerealNone) return visitPrimitiveNone();
    if (value is BasicCerealList) return visitPrimitiveList(value.items);
    if (value is BasicCerealObject) return visitPrimitiveObject(value.items);
    return unreachable("fallthrough in CerealPrimitiveVisitor<$T>");
  }
}

T todo<T>([String message = "todo"]) => throw Exception(message);

T unimplemented<T>([String? message]) => throw UnimplementedError(message);

T unreachable<T>([String? message]) => throw UnreachableError(message);

class UnreachableError {
  final String message;

  const UnreachableError([String? message]) : message = message != null ? "Unreachable: $message" : "Unreachable";
}
