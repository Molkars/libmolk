part of 'lib.dart';

class CerealPrimitiveFormatter with CerealVisitor<String> {
  String format(CerealValue value) => visitCerealValue(value);

  @override
  String defaultVisit() => unreachable();

  @override
  String visitCerealInt(int value) => "$value";

  @override
  String visitCerealDouble(double value) => "$value";

  @override
  String visitCerealString(String value) => '"$value"';

  @override
  String visitCerealBool(bool value) => "$value";

  @override
  String visitCerealList(UnmodifiableListView<CerealValue> items) => visitStrongCerealList(items);

  @override
  String visitCerealObject(UnmodifiableMapView<String, CerealValue> items) => visitStrongCerealObject(items);

  @override
  String visitCerealNull() => "null";

  int _indentLevel = 0;

  String _indent() =>
      _indentLevel.rangeIter().fold(StringBuffer(), (StringBuffer acc, _) => acc..write("  ")).toString();

  @override
  String visitStrongCerealObject(UnmodifiableMapView<String, CerealValue> items) {
    final out = StringBuffer();
    out.write('{');
    final base = _indent();
    _indentLevel++;
    items.iter().enumerate().forEach((index, value) {
      if (index > 0) out.write(',');
      out
        ..write('\n')
        ..write(base)
        ..write("  ");
      out.write('"${value.key}": ${visitCerealValue(value.value)}');
    });
    _indentLevel--;
    if (items.isNotEmpty) {
      out.write('\n');
      out.write(base);
    }

    out.write('}');
    return out.toString();
  }

  @override
  String visitStrongCerealList(UnmodifiableListView<CerealValue> items) {
    final out = StringBuffer();
    out.write('[');
    for (int i = 0; i < items.length; i++) {
      if (i > 0) out.write(', ');
      out.write(visitCerealValue(items[i]));
    }
    out.write(']');
    return out.toString();
  }

  @override
  String visitCerealDateTime(DateTime dateTime) => dateTime.toIso8601String();
}
