import 'package:libmolk_dart/base/result.dart';
import 'package:libmolk_dart/cereal/lib.dart';

class WrappedStrongCerealObjectMill<T extends StrongCerealObject> with CerealMill<T, StrongObjectSchema> {
  final T Function(StrongCerealObject constructor) _factory;

  const WrappedStrongCerealObjectMill(this._factory);

  @override
  Result<T, DeCerealError> deserialize(CerealManager manager, StrongObjectSchema schema, CerealPrimitive value) {
    const inner = StrongCerealObjectMill();
    return inner.deserialize(manager, schema, value).map(_factory);
  }

  @override
  Result<CerealPrimitive, SeCerealError> serialize(CerealManager manager, T value) {
    const inner = StrongCerealObjectMill();
    return inner.serialize(manager, value);
  }
}
