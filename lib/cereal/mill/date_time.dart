part of 'lib.dart';

class CerealDateTimeMill with CerealMill<CerealDateTime, DateTimeSchema> {
  const CerealDateTimeMill();

  @override
  Result<CerealDateTime, DeCerealError> deserialize(
      CerealManager manager, DateTimeSchema schema, CerealPrimitive value) {
    return value.cerealMap(
      string: (val) => Result.wrap<DateTime, FormatException>(() => DateTime.parse(val.value))
          .mapErr((err) => DeCerealError("DateTimeMill: $err"))
          .map(CerealDateTime.new),
      orElse: (val) => Result.err(DeCerealError.expected(CerealString, val.runtimeType)),
    );
  }

  @override
  Result<CerealPrimitive, SeCerealError> serialize(CerealManager manager, CerealDateTime value) {
    return Result.ok(CerealString(value.value.toIso8601String()));
  }
}
