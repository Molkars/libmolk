part of 'lib.dart';

class CerealTupleMill with CerealMill<CerealTuple, TupleSchema> {
  const CerealTupleMill();

  @override
  Result<CerealTuple, DeCerealError> deserialize(CerealManager manager, TupleSchema schema, CerealPrimitive value) {
    return value.cerealMap(
      list: (val) {
        if (val.length != schema.types.length) {
          return Result.err(
            DeCerealError(
              "CerealTupleMill: length of value $value did not match length of tuple elements in $schema",
            ),
          );
        }

        return val.items.iter()
            .zip(schema.types.iter())
            .map((value, schema) => manager.deserialize(schema, value))
            .collect(const ExtractResultCollector(ListCollector()))
            .map(CerealTuple.new);
      },
      orElse: (val) => Result.err(DeCerealError.expected(BasicCerealList, val.runtimeType)),
    );
  }

  @override
  Result<CerealPrimitive, SeCerealError> serialize(CerealManager manager, CerealTuple value) {
    return value.value
        .iter()
        .map((val) => manager.serialize(val))
        .collect(const ExtractResultCollector(ListCollector()))
        .map(BasicCerealList.new);
  }
}