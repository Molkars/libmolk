part of 'lib.dart';

class StrongCerealListMill with CerealMill<StrongCerealList, StrongListSchema> {
  const StrongCerealListMill();

  @override
  Result<StrongCerealList, DeCerealError> deserialize(CerealManager manager, StrongListSchema schema, CerealPrimitive value) {
    return value.cerealMap(
        list: (val) {
          final type = schema.type;
          return val.items
              .iter()
              .map((e) => manager.deserialize(type, e))
              .collect(const ExtractResultCollector(ListCollector()))
              .map((vals) => StrongCerealList(schema, vals));
        },
        orElse: (val) => Result.err(DeCerealError.expected(BasicCerealList, val.runtimeType)),
    );
  }

  @override
  Result<CerealPrimitive, SeCerealError> serialize(CerealManager manager, StrongCerealList value) {
    return value.value
        .iter()
        .map((elem) => manager.serialize(elem))
        .collect(const ExtractResultCollector(ListCollector()))
        .map(BasicCerealList.new);
  }
}