part of 'lib.dart';

class BasicListMill with CerealMill<BasicCerealList, BasicListSchema> {
  const BasicListMill();

  @override
  Result<BasicCerealList, DeCerealError> deserialize(
      CerealManager manager, BasicListSchema schema, CerealPrimitive value) {
    return value.cerealMap(
      list: (val) => Result.block((tri) {
        final values = val.items
            .iter()
            .map((e) => manager.deserialize(e.getSchema(), e).tri(tri))
            .tryCast<CerealPrimitive>()
            .map((e) => e.okOrElse(() => DeCerealError("Basic cereal list can only contain primitives: $e is not a primitive")))
            .collect(const ExtractResultCollector(ListCollector<CerealPrimitive>()));
        return values.map(BasicCerealList.new);
      }),
      orElse: (val) => Result.err(DeCerealError.expected(BasicCerealList, val.concreteType)),
    );
  }

  @override
  Result<BasicCerealList, SeCerealError> serialize(CerealManager manager, BasicCerealList value) {
    return value.items
        .iter()
        .map(manager.serialize)
        .map((val) => val.tryCastOrElse((val) => SeCerealError("Value $val must be a primitive value")))
        .collect(ExtractResultCollector(ListCollector<CerealPrimitive>()))
        .map(BasicCerealList.new);
  }
}
