part of 'lib.dart';

class CerealBoolMill with CerealMill<CerealBool, BoolSchema> {
  const CerealBoolMill();

  @override
  Result<CerealBool, DeCerealError> deserialize(CerealManager manager, BoolSchema schema, CerealPrimitive value) {
    return value.cerealMap(
      bool: (val) => Result.ok(CerealBool(val.value)),
      orElse: (val) => Result.err(DeCerealError.expected(CerealBool, val.runtimeType)),
    );
  }

  @override
  Result<CerealPrimitive, SeCerealError> serialize(CerealManager manager, CerealBool value) {
    return Result.ok(CerealBool(value.value));
  }
}