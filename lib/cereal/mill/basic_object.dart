part of 'lib.dart';

class BasicObjectMill with CerealMill<BasicCerealObject, BasicObjectSchema> {
  const BasicObjectMill();

  @override
  Result<BasicCerealObject, DeCerealError> deserialize(
    CerealManager manager,
    BasicObjectSchema schema,
    CerealPrimitive value,
  ) {
    return value.cerealMap(
      object: (val) => Result.block((tri) {
        final values = val.items.iter().map((key, value) {
          final val = manager.deserialize(value.getSchema(), value).tri(tri);

          return Option.some(val)
              .tryCast<CerealPrimitive>()
              .okOrElse(() => DeCerealError("Basic cereal object can only contain primitives: $val is not a primitive"))
              .map((val) => pair(key, val));
        }).collect(const ExtractResultCollector(MapCollector()));

        return values.map(BasicCerealObject.new);
      }),
      orElse: (val) => Result.err(DeCerealError.expected(BasicCerealObject, val.concreteType)),
    );
  }

  @override
  Result<CerealPrimitive, SeCerealError> serialize(CerealManager manager, BasicCerealObject value) {
    // TODO: implement serialize
    throw UnimplementedError();
  }
}
