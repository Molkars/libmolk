part of 'lib.dart';

class CerealArrayMill with CerealMill<CerealArray, ArraySchema> {
  const CerealArrayMill();

  @override
  Result<CerealArray, DeCerealError> deserialize(CerealManager manager, ArraySchema schema, CerealPrimitive value) {
    return value.cerealMap(
      list: (val) =>
          Result.block((tri) {
            if (val.length != schema.length) {
              final error = DeCerealError(
                  "Expected array of length ${schema.length}, instead got array of length ${val.length}");
              return Result.err(error);
            }

            final values = val.items.map((elem) {
              final type = schema.schema.unwrap();
              return manager.deserialize(type, elem).tri(tri);
            }).toList();

            return Result.ok(CerealArray(values));
          }),
      orElse: (val) => Result.err(DeCerealError.expected(BasicCerealList, val.concreteType)),
    );
  }

  @override
  Result<CerealPrimitive, SeCerealError> serialize(CerealManager manager, CerealArray value) {
    return Result.block((tri) {
      final values = value.value.map((e) => manager.serialize(e).tri(tri)).toList();
      return Result.ok(BasicCerealList(values));
    });
  }
}