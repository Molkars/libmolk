part of 'lib.dart';

class WeakCerealObjectMill with CerealMill<WeakCerealObject, WeakObjectSchema> {
  const WeakCerealObjectMill();

  @override
  Result<WeakCerealObject, DeCerealError> deserialize(
    CerealManager manager,
    WeakObjectSchema schema,
    CerealPrimitive value,
  ) {
    return value.cerealMap(
      object: (val) => val
          .iter()
          .map((key, value) => manager.deserialize(value.getSchema(), value).map((val) => pair(key, val)))
          .collect(ExtractResultCollector(MapCollector()))
          .map(WeakCerealObject.new),
      orElse: (val) => Result.err(DeCerealError.expected(BasicCerealObject, val.runtimeType)),
    );
  }

  @override
  Result<CerealPrimitive, SeCerealError> serialize(CerealManager manager, WeakCerealObject value) {
    return value.value
        .iter()
        .map((key, value) => manager.serialize(value).map((val) => pair(key, val)))
        .collect(ExtractResultCollector(MapCollector()))
        .map(BasicCerealObject.new);
  }
}
