part of 'lib.dart';

class CerealStringMill with CerealMill<CerealString, StringSchema> {
  const CerealStringMill();

  @override
  Result<CerealString, DeCerealError> deserialize(CerealManager manager, StringSchema schema, CerealPrimitive value) {
    return value.cerealMap(
      string: (val) => Result.ok(CerealString(val.value)),
      orElse: (val) => Result.err(DeCerealError.expected(CerealString, val.runtimeType)),
    );
  }

  @override
  Result<CerealPrimitive, SeCerealError> serialize(CerealManager manager, CerealString value) {
    return Result.ok(CerealString(value.value));
  }

}