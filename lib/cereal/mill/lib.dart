library libmolk.cereal.mill;

import 'package:libmolk_dart/base/result.dart';
import 'package:libmolk_dart/cereal/lib.dart';
import 'package:libmolk_dart/iter/iter.dart';
import 'package:libmolk_dart/iter/pair_iter.dart';

part 'array.dart';

part 'basic_list.dart';

part 'basic_object.dart';

part 'bool.dart';

part 'date_time.dart';

part 'double.dart';

part 'int.dart';

part 'none.dart';

part 'option.dart';

part 'string.dart';

part 'strong_list.dart';

part 'strong_object.dart';

part 'tuple.dart';

part 'weak_list.dart';

part 'weak_object.dart';

mixin CerealMill<V extends CerealValue, S extends CerealSchema> {
  Result<V, DeCerealError> deserialize(CerealManager manager, S schema, CerealPrimitive value);

  Result<CerealPrimitive, SeCerealError> serialize(CerealManager manager, V value);
}

class CerealManager {
  final Map<Type, CerealMill> _encoders; // ValueType -> Encoder
  final Map<Type, CerealMill> _decoders; // SchemaType -> Decoder

  const CerealManager({
    Map<Type, CerealMill> encoders = defaultEncoders,
    Map<Type, CerealMill> decoders = defaultDecoders,
  })  : _encoders = encoders,
        _decoders = decoders;

  void register<T extends CerealValue, S extends CerealSchema>(CerealMill<T, S> mill) {
    if (_encoders.containsKey(T) || _decoders.containsKey(S)) {
      throw StateError("CerealMill<$T, $S> already registered in CerealManager");
    }

    _encoders[T] = mill;
    _decoders[S] = mill;
  }

  Result<CerealValue, DeCerealError> deserialize<S extends CerealSchema>(S schema, CerealPrimitive value) {
    final decoder = _decoders[S == CerealSchema ? schema.runtimeType : S];
    if (decoder == null) return Result.err(DeCerealError("No decoder found for the schema $S"));
    return decoder.deserialize(this, schema, value);
  }

  Result<CerealPrimitive, SeCerealError> serialize<V extends CerealValue>(V value) {
    final encoder = _encoders[V == CerealValue ? value.runtimeType : V];
    if (encoder == null) return Result.err(SeCerealError("No encoder for found the value type $V"));
    return encoder.serialize(this, value);
  }

  static const defaultEncoders = <Type, CerealMill>{
    // Value -> Primitive
    CerealArray: CerealArrayMill(),
    BasicCerealList: BasicListMill(),
    BasicCerealObject: BasicObjectMill(),
    CerealBool: CerealBoolMill(),
    CerealDateTime: CerealDateTimeMill(),
    CerealDouble: CerealDoubleMill(),
    CerealInt: CerealIntMill(),
    CerealNone: CerealNoneMill(),
    CerealOption: CerealOptionMill(),
    CerealString: CerealStringMill(),
    StrongCerealList: StrongCerealListMill(),
    StrongCerealObject: StrongCerealObjectMill(),
    CerealTuple: CerealTupleMill(),
    WeakCerealList: WeakCerealListMill(),
    WeakCerealObject: WeakCerealObjectMill(),
  };
  static const defaultDecoders = <Type, CerealMill>{
    ArraySchema: CerealArrayMill(),
    BasicListSchema: BasicListMill(),
    BasicObjectSchema: BasicObjectMill(),
    BoolSchema: CerealBoolMill(),
    DateTimeSchema: CerealDateTimeMill(),
    DoubleSchema: CerealDoubleMill(),
    IntSchema: CerealIntMill(),
    NoneSchema: CerealNoneMill(),
    OptionSchema: CerealOptionMill(),
    StringSchema: CerealStringMill(),
    StrongListSchema: StrongCerealListMill(),
    StrongObjectSchema: StrongCerealObjectMill(),
    TupleSchema: CerealTupleMill(),
    WeakListSchema: WeakCerealListMill(),
    WeakObjectSchema: WeakCerealObjectMill(),
  }; // shh
}

class DeCerealError implements Exception {
  final String msg;

  const DeCerealError(String msg) : msg = "Deserialization Error: $msg";

  const DeCerealError.endOfInput() : this("Unexpected end of input");

  DeCerealError.expectedString(Option<int> byte, List<int> actual)
      : this(
            "Expected ${actual.map(String.fromCharCode).map((s) => "`$s`").join(" or ")}, instead got ${byte.map(String.fromCharCode).map((val) => "`$val`")}");

  DeCerealError.expected(Type type, Type actual) : this("Expected type `$type`, instead found `$actual`");

  @override
  String toString() {
    return 'DeCerealError{msg: $msg}';
  }
}

class SeCerealError implements Exception {
  final String msg;

  const SeCerealError(String msg) : msg = "Serialization Error - $msg";

  @override
  String toString() => 'SeCerealError{msg: $msg}';
}
