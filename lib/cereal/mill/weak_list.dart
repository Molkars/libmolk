part of 'lib.dart';

class WeakCerealListMill with CerealMill<WeakCerealList, WeakListSchema> {
  const WeakCerealListMill();

  @override
  Result<WeakCerealList, DeCerealError> deserialize(
    CerealManager manager,
    WeakListSchema schema,
    CerealPrimitive value,
  ) {
    return value.cerealMap(
      list: (val) => val.items
          .iter()
          .map((val) => manager.deserialize(val.getSchema(), val))
          .collect(const ExtractResultCollector(ListCollector()))
          .map(WeakCerealList.new),
      orElse: (val) => Result.err(DeCerealError.expected(BasicCerealList, val.runtimeType)),
    );
  }

  @override
  Result<BasicCerealList, SeCerealError> serialize(CerealManager manager, WeakCerealList value) {
    return value.value
        .iter()
        .map((val) => manager.serialize(val))
        .collect(const ExtractResultCollector(ListCollector()))
        .map(BasicCerealList.new);
  }
}
