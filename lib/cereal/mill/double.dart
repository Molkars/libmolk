part of 'lib.dart';

class CerealDoubleMill with CerealMill<CerealDouble, DoubleSchema> {
  const CerealDoubleMill();

  @override
  Result<CerealDouble, DeCerealError> deserialize(CerealManager manager, DoubleSchema schema, CerealPrimitive value) {
    return value.cerealMap(
      double: (val) => Result.ok(CerealDouble(val.value)),
      orElse: (val) => Result.err(DeCerealError.expected(CerealDouble, val.runtimeType)),
    );
  }

  @override
  Result<CerealPrimitive, SeCerealError> serialize(CerealManager manager, CerealDouble value) {
    return Result.ok(CerealDouble(value.value));
  }

}