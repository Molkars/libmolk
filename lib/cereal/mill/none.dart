part of 'lib.dart';

class CerealNoneMill with CerealMill<CerealNone, NoneSchema> {
  const CerealNoneMill();

  @override
  Result<CerealNone, DeCerealError> deserialize(CerealManager manager, NoneSchema schema, CerealPrimitive value) {
    return value.cerealMap(
      none: () => const Result.ok(CerealNone()),
      orElse: (val) => Result.err(DeCerealError.expected(CerealNone, val.runtimeType)),
    );
  }

  @override
  Result<CerealPrimitive, SeCerealError> serialize(CerealManager manager, CerealNone value) {
    return const Result.ok(CerealNone());
  }
}