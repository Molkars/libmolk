part of 'lib.dart';

class StrongCerealObjectMill with CerealMill<StrongCerealObject, StrongObjectSchema> {
  const StrongCerealObjectMill();

  @override
  Result<StrongCerealObject, DeCerealError> deserialize(
    CerealManager manager,
    StrongObjectSchema schema,
    CerealPrimitive value,
  ) {
    return value.cerealMap(
      object: (val) => Result.block((tri) {
        if (val.keys.length != schema.fields.keys.length) {
          return Result.err(DeCerealError(
              "StrongCerealObjectMill: Tried to parse value with different number of keys than schema: $val - $schema"));
        }

        final out = <String, CerealValue>{};
        for (final pair in val.items.entries) {
          if (!schema.fields.containsKey(pair.key)) {
            return Result.err(DeCerealError("StrongCerealObjectMill: Key ${pair.key} was not found in schema!"));
          }
          final value = manager.deserialize(schema.fields[pair.key]!, pair.value).tri(tri);
          out[pair.key] = value;
        }

        return Result.ok(StrongCerealObject(schema, out));
      }),
      orElse: (val) => Result.err(DeCerealError.expected(BasicCerealObject, val.runtimeType)),
    );
  }

  @override
  Result<CerealPrimitive, SeCerealError> serialize(CerealManager manager, StrongCerealObject value) {
    return Result.block((tri) {
      final out = <String, CerealPrimitive>{};

      for (final pair in value.value.entries) {
        out[pair.key] = manager.serialize(pair.value).tri(tri);
      }

      return Result.ok(BasicCerealObject(out));
    });
  }
}
