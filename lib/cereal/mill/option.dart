part of 'lib.dart';

class CerealOptionMill with CerealMill<CerealOption, OptionSchema> {
  const CerealOptionMill();

  @override
  Result<CerealOption, DeCerealError> deserialize(CerealManager manager, OptionSchema schema, CerealPrimitive value) {
    return value.cerealMap(
      none: () => Result.ok(CerealOption.none()),
      orElse: (val) {
        final value = manager.deserialize(schema, val);
        return value.tryCastOrElse((val) => DeCerealError("Deserializer returned non-cereal option: $val"));
      },
    );
  }

  @override
  Result<CerealPrimitive, SeCerealError> serialize(CerealManager manager, CerealOption value) {
    if (value.inner.isSome) {
      final inner = value.inner.unwrap();
      return manager.serialize(inner);
    } else {
      return Result.ok(const CerealNone());
    }
  }
}
