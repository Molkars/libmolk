part of 'lib.dart';

class CerealIntMill with CerealMill<CerealInt, IntSchema> {
  const CerealIntMill();

  @override
  Result<CerealInt, DeCerealError> deserialize(CerealManager manager, IntSchema schema, CerealPrimitive value) {
    return value.cerealMap(
      int: (val) => Result.ok(CerealInt(val.value)),
      orElse: (val) => Result.err(DeCerealError.expected(CerealInt, val.runtimeType)),
    );
  }

  @override
  Result<CerealPrimitive, SeCerealError> serialize(CerealManager manager, CerealInt value) {
    return Result.ok(CerealInt(value.value));
  }
}