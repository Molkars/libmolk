import 'package:libmolk_dart/base/ascii.dart';
import 'package:libmolk_dart/base/cursor.dart';
import 'package:libmolk_dart/base/result.dart';
import 'package:libmolk_dart/cereal/lib.dart';
import 'package:libmolk_dart/iter/iter.dart';

class JsonParser {
  final Cursor _cursor;

  JsonParser(List<int> bytes) : _cursor = Cursor(bytes);

  DeCerealError _eoi() => DeCerealError.endOfInput();

  Result<int, DeCerealError> _take(int char) {
    return _cursor
        .next()
        .okOrElse<DeCerealError>(_eoi)
        .filterOrElse((val) => val == char, (val) => DeCerealError.expectedString(some(val), [char]));
  }

  bool _doTake(int char) {
    return _cursor.peek().filter((val) => val == char).map((val) => _cursor.next()).isSome;
  }

  void _clearSpace() {
    while (_cursor.peek().filter(Ascii.isWhitespace).isSome) {
      _cursor.next();
    }
  }

  Result<int, DeCerealError> _next() => _cursor.next().okOrElse(_eoi);

  Result<int, DeCerealError> _peek() => _cursor.peek().okOrElse(_eoi);

  List<int> _consumeWhile(bool Function(int char) f) {
    final out = <int>[];
    while (_cursor.peek().filter(f).isSome) {
      out.add(_cursor.next().unwrap());
    }
    return out;
  }

  DeCerealError _expected(List<int> expected) {
    assert(_cursor.peek().filter(expected.contains).isNone);
    return DeCerealError.expectedString(_cursor.next(), expected);
  }

  Result<CerealPrimitive, DeCerealError> parse() {
    _clearSpace();
    if (_cursor.match(Ascii.leftBrace)) return _object();
    if (_cursor.match(Ascii.leftBracket)) return _list();
    return Result.err(_expected([Ascii.leftBrace, Ascii.leftBracket]));
  }

  bool _consumeStr(String value) {
    return _cursor
        .sliceForward(value.length)
        .map(String.fromCharCodes)
        .filter((str) => str == value)
        .map((_) => _cursor.skip(value.length))
        .isSome;
  }

  Result<CerealPrimitive, DeCerealError> _value() {
    return Result.block((tri) {
      final char = _peek().tri(tri);

      switch (char) {
        case Ascii.leftBrace:
          return _object();
        case Ascii.leftBracket:
          return _list();
        case Ascii.quotation:
          return _string();
        default:
          if (char == Ascii.dash || Ascii.isNumeric(char)) return _number();
          if (_consumeStr("true")) return const Result.ok(CerealBool(true));
          if (_consumeStr("false")) return const Result.ok(CerealBool(false));
          if (_consumeStr("null")) return const Result.ok(CerealNone());
          return Result.err(DeCerealError("Expected number or true or false or null, instead got $char"));
      }
    });
  }

  Result<CerealPrimitive, DeCerealError> _number() {
    return Result.block((tri) {
      final List<int> bytes = [];
      if (_doTake(Ascii.dash)) bytes.add(Ascii.dash);
      bytes.addAll(_consumeWhile(Ascii.isNumeric));

      if (bytes.isEmpty) {
        return Result.err(_expected(Ascii.numeric));
      }

      if (!_doTake(Ascii.period)) {
        final string = String.fromCharCodes(bytes);
        final number = Result.wrap<int, FormatException>(() => int.parse(string))
            .mapErr((err) => DeCerealError("Unable to parse $string: $err"))
            .tri(tri);

        return Result.ok(CerealInt(number));
      }

      bytes.add(Ascii.period);
      bytes.addAll(_consumeWhile(Ascii.isNumeric));

      if (_doTake(Ascii.letterE) || _doTake(Ascii.capitalE)) {
        bytes.add(Ascii.letterE);

        if (_doTake(Ascii.dash)) bytes.add(Ascii.dash);

        final List<int> power = _consumeWhile(Ascii.isNumeric);
        if (power.isEmpty) return Result.err(_expected(Ascii.numeric));

        bytes.addAll(power);
      }

      final string = String.fromCharCodes(bytes);
      final number = Result.wrap<double, FormatException>(() => double.parse(string))
          .mapErr((err) => DeCerealError("Unable to parse $string: $err"))
          .tri(tri);
      return Result.ok(CerealDouble(number));
    });
  }

  Result<CerealString, DeCerealError> _string() {
    return Result.block((tri) {
      _take(Ascii.quotation).tri(tri);

      final out = StringBuffer();
      while (!_doTake(Ascii.quotation)) {
        final char = _next().tri(tri);

        if (Ascii.isControl(char)) {
          return Result.err(DeCerealError("Un-allowed character ascii: $char found"));
        }

        if (char != Ascii.backslash) {
          out.write(String.fromCharCode(char));
          continue;
        }

        final int escape = _next().tri(tri);

        int byte;
        switch (escape) {
          case Ascii.quotation:
          case Ascii.backslash:
          case Ascii.slash:
            byte = escape;
            break;
          case Ascii.letterB:
            byte = Ascii.bs;
            break;
          case Ascii.letterF:
            byte = Ascii.ff;
            break;
          case Ascii.letterN:
            byte = Ascii.lf;
            break;
          case Ascii.letterR:
            byte = Ascii.cr;
            break;
          case Ascii.letterT:
            byte = Ascii.tab;
            break;
          case Ascii.letterU:
            byte = 0;
            for (int i = 0; i < 4; i++) {
              final char = _next()
                  .filterOrElse(
                      Ascii.isHexadecimal, (byte) => DeCerealError.expectedString(Option.some(byte), Ascii.hexadecimal))
                  .tri(tri);
              final int value = int.parse(String.fromCharCode(char), radix: 16);
              byte |= value << 4 * (3 - i);
            }
            break;
          default:
            return Result.err(DeCerealError.expectedString(
              Option.some(escape),
              [
                Ascii.quotation,
                Ascii.backslash,
                Ascii.slash,
                Ascii.letterB,
                Ascii.letterF,
                Ascii.letterN,
                Ascii.letterR,
                Ascii.letterT,
                Ascii.letterU
              ],
            ));
        }

        out.write(String.fromCharCode(byte));
      }

      return Result.ok(CerealString(out.toString()));
    });
  }

  Result<BasicCerealList, DeCerealError> _list() {
    return Result.block((tri) {
      _take(Ascii.leftBracket).tri(tri);
      _clearSpace();

      if (_doTake(Ascii.rightBracket)) return const Result.ok(BasicCerealList([]));

      final values = <CerealPrimitive>[];

      while (true) {
        print('getting-value');
        final value = _value().tri(tri);
        print('got: $value');
        values.add(value);
        _clearSpace();

        if (!_doTake(Ascii.comma)) {
          _take(Ascii.rightBracket).tri(tri);
          break;
        }
        _clearSpace();
      }

      return Result.ok(BasicCerealList(values));
    });
  }

  Result<BasicCerealObject, DeCerealError> _object() {
    return Result.block((tri) {
      _take(Ascii.leftBrace).tri(tri);
      _clearSpace();

      if (_doTake(Ascii.rightBrace)) return const Result.ok(BasicCerealObject({}));

      final fields = <String, CerealPrimitive>{};
      while (true) {
        final key = _string().tri(tri);
        _clearSpace();

        _take(Ascii.colon).tri(tri);
        _clearSpace();
        final value = _value().tri(tri);
        _clearSpace();

        print('setting: ${key.value} to $value');
        fields[key.value] = value;

        if (!_doTake(Ascii.comma)) {
          _take(Ascii.rightBrace);
          break;
        }
        _clearSpace();
      }

      return Result.ok(BasicCerealObject(fields));
    });
  }
}

class JsonEmitter {
  final CerealManager manager;
  final int space;

  const JsonEmitter(this.manager, [this.space = 0]) : assert(space >= 0);

  Result<String, SeCerealError> emit<T extends CerealValue>(T value) {
    return manager
        .serialize<T>(value)
        .map((encoded) => _JsonEmitter(space, space == 0 ? "" : "\n").visitCerealPrimitive(encoded));
  }
}

class _JsonEmitter with CerealPrimitiveVisitor<String> {
  final int space;
  final String newline;

  _JsonEmitter(this.space, this.newline);

  @override
  String visitPrimitiveInt(int value) => "$value";

  @override
  String visitPrimitiveDouble(double value) => "$value";

  @override
  String visitPrimitiveString(String value) {
    String hex4(int byte) {
      assert(byte <= 0xFFFF);
      final a = byte & 0xF;
      final b = (byte & 0xF0) >> 4;
      final c = (byte & 0xF00) >> 8;
      final d = (byte & 0xF000) >> 12;
      const bytes = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
      return bytes[d] + bytes[c] + bytes[b] + bytes[a];
    }

    final out = StringBuffer('"');
    for (final byte in value.codeUnits) {
      if (byte == Ascii.quotation)
        out.write(r'\"');
      else if (byte == Ascii.lf)
        out.write(r"\n");
      else if (byte == Ascii.backslash)
        out.write(r"\\");
      else if (byte == Ascii.apostrophe)
        out.write(r"\'");
      else if (byte == Ascii.bs)
        out.write(r"\b");
      else if (byte == Ascii.ff)
        out.write(r"\f");
      else if (byte == Ascii.cr)
        out.write(r"\r");
      else if (byte == Ascii.tab)
        out.write(r"\t");
      else if (byte == Ascii.slash)
        out.write(r"\/");
      else if (Ascii.invalid(byte))
        out.write("\\u${hex4(byte)}");
      else
        out.write(String.fromCharCode(byte));
    }
    out.write('"');
    return out.toString();
  }

  @override
  String visitPrimitiveBool(bool value) => "$value";

  @override
  String visitPrimitiveList(UnmodifiableListView<CerealPrimitive> items) =>
      "[${items.map(visitCerealPrimitive).join(",${space == 0 ? "" : " "}")}]";

  int _tab = 0;

  String _space() => String.fromCharCodes(List.filled(_tab * space, Ascii.space));

  @override
  String visitPrimitiveObject(UnmodifiableMapView<String, CerealPrimitive> items) {
    final out = StringBuffer("{");

    _tab++;
    for (final pair in items.entries.iter().enumerate().iterable()) {
      if (pair.left > 0) out.write(',');
      out
        ..write(newline)
        ..write(_space())
        ..write(visitPrimitiveString(pair.right.key))
        ..write(":")
        ..write(space == 0 ? "" : " ")
        ..write(visitCerealPrimitive(pair.right.value));
    }
    _tab--;

    out
      ..write(newline)
      ..write(_space())
      ..write('}');
    return out.toString();
  }

  @override
  String visitPrimitiveNone() => "null";
}
