part of 'lib.dart';

class CerealNone extends CerealPrimitive {
  const CerealNone() : super._();

  @override
  NoneSchema getSchema() => const NoneSchema();

  @override
  Type get concreteType => CerealNone;

  @override
  T cerealMap<T>({
    T Function(CerealInt val)? int,
    T Function(CerealDouble val)? double,
    T Function(CerealString val)? string,
    T Function(CerealBool val)? bool,
    T Function()? none,
    T Function(BasicCerealList val)? list,
    T Function(BasicCerealObject val)? object,
    T Function(CerealPrimitive val)? orElse,
  }) =>
      none?.call() ?? orElse?.call(this) ?? unreachable("[$concreteType#map<$T>] no function to handle case");

  @override
  bool operator ==(Object other) => identical(this, other) || other is CerealNone && runtimeType == other.runtimeType;

  @override
  int get hashCode => 0;
}
