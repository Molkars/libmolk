library libmolk.cereal.primitive;

import 'dart:collection';

import 'package:libmolk_dart/base/clone.dart';
import 'package:libmolk_dart/cereal/lib.dart';
import 'package:libmolk_dart/iter/iter.dart';

part 'bool.dart';
part 'double.dart';
part 'int.dart';
part 'none.dart';
part 'string.dart';
part 'basic_list.dart';
part 'basic_object.dart';

abstract class CerealPrimitive with CerealValue {
  const CerealPrimitive._();

  T cerealMap<T>({
    T Function(CerealInt val)? int,
    T Function(CerealDouble val)? double,
    T Function(CerealString val)? string,
    T Function(CerealBool val)? bool,
    T Function()? none,
    T Function(BasicCerealList val)? list,
    T Function(BasicCerealObject val)? object,
    T Function(CerealPrimitive val)? orElse,
  });
}

bool _mapEq<K, V>(Map<K, V> a, Map<K, V> b) => a.keys.toSet() == b.keys.toSet() && a.keys.every((k) => a[k]! == b[k]!);
bool _listEq<E>(List<E> a, List<E> b) => a.length == b.length && a.iter().zip(b.iter()).every((a, b) => a == b);
bool _setEq<E>(Set<E> a, Set<E> b) => a.length == b.length && a.every(b.contains);