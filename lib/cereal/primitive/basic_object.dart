part of 'lib.dart';

class BasicCerealObject extends CerealPrimitive with MapMixin<String, CerealPrimitive> {
  final Map<String, CerealPrimitive> _inner;

  UnmodifiableMapView<String, CerealPrimitive> get items => UnmodifiableMapView(_inner);

  const BasicCerealObject(this._inner) : super._();

  @override
  Type get concreteType => BasicCerealObject;

  Map<String, CerealSchema> getEntryTypes() => _inner.map((key, value) => MapEntry(key, value.getSchema()));

  @override
  CerealSchema getSchema() => BasicObjectSchema(getEntryTypes());

  @override
  T cerealMap<T>({
    T Function(CerealInt val)? int,
    T Function(CerealDouble val)? double,
    T Function(CerealString val)? string,
    T Function(CerealBool val)? bool,
    T Function()? none,
    T Function(BasicCerealList val)? list,
    T Function(BasicCerealObject val)? object,
    T Function(CerealPrimitive val)? orElse,
  }) =>
      object?.call(this) ??
      orElse?.call(this) ??
      unreachable("[$concreteType#map<$T>] no function to handle case object in $runtimeType");

  @override
  CerealPrimitive? operator [](Object? key) => _inner[key];

  @override
  void operator []=(String key, CerealPrimitive value) => _inner[key] = value;

  @override
  void clear() => _inner.clear();

  @override
  Iterable<String> get keys => _inner.keys;

  @override
  CerealPrimitive? remove(Object? key) => _inner.remove(key);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BasicCerealObject && runtimeType == other.runtimeType && _mapEq(_inner, other._inner) ||
      other is StrongCerealObject && _mapEq(other.value, items);

  @override
  int get hashCode => _inner.hashCode;
}
