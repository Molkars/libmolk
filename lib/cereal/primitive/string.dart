part of libmolk.cereal.primitive;

class CerealString extends CerealPrimitive {
  final String value;

  const CerealString(this.value) : super._();

  @override
  StringSchema getSchema() => const StringSchema();

  @override
  Type get concreteType => CerealString;

  @override
  T cerealMap<T>({
    T Function(CerealInt val)? int,
    T Function(CerealDouble val)? double,
    T Function(CerealString val)? string,
    T Function(CerealBool val)? bool,
    T Function()? none,
    T Function(BasicCerealList val)? list,
    T Function(BasicCerealObject val)? object,
    T Function(CerealPrimitive val)? orElse,
  }) =>
      string?.call(this) ?? orElse?.call(this) ?? unreachable("[$concreteType#map<$T>] no function to handle case");

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is CerealString && runtimeType == other.runtimeType && value == other.value;

  @override
  int get hashCode => value.hashCode;
}
