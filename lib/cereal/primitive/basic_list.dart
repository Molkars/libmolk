part of 'lib.dart';

class BasicCerealList extends CerealPrimitive with ListMixin<CerealPrimitive> {
  final List<CerealPrimitive> _inner;

  UnmodifiableListView<CerealPrimitive> get items => UnmodifiableListView(_inner);

  const BasicCerealList(this._inner) : super._();

  @override
  BasicListSchema getSchema() => BasicListSchema(getElementTypes());

  @override
  Type get concreteType => BasicCerealList;

  List<CerealSchema> getElementTypes() => _inner.map((e) => e.getSchema()).toList();

  @override
  T cerealMap<T>({
    T Function(CerealInt val)? int,
    T Function(CerealDouble val)? double,
    T Function(CerealString val)? string,
    T Function(CerealBool val)? bool,
    T Function()? none,
    T Function(BasicCerealList val)? list,
    T Function(BasicCerealObject val)? object,
    T Function(CerealPrimitive val)? orElse,
  }) =>
      list?.call(this) ??
      orElse?.call(this) ??
      unreachable("[$concreteType#map<$T>] no function to handle case list in $runtimeType");

  @override
  int get length => _inner.length;

  @override
  set length(int val) => _inner.length = val;

  @override
  CerealPrimitive operator [](int index) => _inner[index];

  @override
  void operator []=(int index, CerealPrimitive value) => _inner[index] = value;

  @override
  bool operator ==(Object other) =>
      identical(this, other)
          || other is BasicCerealList && runtimeType == other.runtimeType && _listEq(_inner, other._inner)
          || other is StrongListSchema && items.iter().every((e) => e.getSchema() == other.type);

  @override
  int get hashCode => _inner.hashCode;
}
