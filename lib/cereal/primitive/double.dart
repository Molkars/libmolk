part of libmolk.cereal.primitive;

class CerealDouble extends CerealPrimitive {
  final double value;

  const CerealDouble(this.value) : super._();

  @override
  DoubleSchema getSchema() => const DoubleSchema();

  @override
  Type get concreteType => CerealDouble;

  @override
  T cerealMap<T>({
    T Function(CerealInt val)? int,
    T Function(CerealDouble val)? double,
    T Function(CerealString val)? string,
    T Function(CerealBool val)? bool,
    T Function()? none,
    T Function(BasicCerealList val)? list,
    T Function(BasicCerealObject val)? object,
    T Function(CerealPrimitive val)? orElse,
  }) =>
      double?.call(this) ?? orElse?.call(this) ?? unreachable("[$concreteType#map<$T>] no function to handle case");

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is CerealDouble && runtimeType == other.runtimeType && value == other.value;

  @override
  int get hashCode => value.hashCode;
}
