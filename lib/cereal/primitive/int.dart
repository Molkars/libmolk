part of libmolk.cereal.primitive;

class CerealInt extends CerealPrimitive {
  final int value;

  const CerealInt(this.value) : super._();

  @override
  CerealSchema getSchema() => const IntSchema();

  @override
  Type get concreteType => CerealInt;

  @override
  T cerealMap<T>({
    T Function(CerealInt val)? int,
    T Function(CerealDouble val)? double,
    T Function(CerealString val)? string,
    T Function(CerealBool val)? bool,
    T Function()? none,
    T Function(BasicCerealList val)? list,
    T Function(BasicCerealObject val)? object,
    T Function(CerealPrimitive val)? orElse,
  }) =>
      int?.call(this) ?? orElse?.call(this) ?? unreachable("[$concreteType#map<$T>] no function to handle case");

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is CerealInt && runtimeType == other.runtimeType && value == other.value;

  @override
  int get hashCode => value.hashCode;
}
