part of libmolk.cereal.primitive;

class CerealBool extends CerealPrimitive {
  final bool value;

  const CerealBool(this.value) : super._();

  @override
  BoolSchema getSchema() => const BoolSchema();

  @override
  Type get concreteType => CerealBool;

  @override
  T cerealMap<T>({
    T Function(CerealInt val)? int,
    T Function(CerealDouble val)? double,
    T Function(CerealString val)? string,
    T Function(CerealBool val)? bool,
    T Function()? none,
    T Function(BasicCerealList val)? list,
    T Function(BasicCerealObject val)? object,
    T Function(CerealPrimitive val)? orElse,
  }) =>
      bool?.call(this) ?? orElse?.call(this) ?? unreachable("[$concreteType#map<$T>] no function to handle case");

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is CerealBool && runtimeType == other.runtimeType && value == other.value;

  @override
  int get hashCode => value.hashCode;
}
