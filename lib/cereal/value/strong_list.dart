part of 'lib.dart';

class StrongCerealList with CerealValue, ListMixin<CerealValue> {
  CerealListSchema _schema;
  final List<CerealValue> _inner;

  UnmodifiableListView<CerealValue> get value => UnmodifiableListView(_inner);

  CerealListSchema get schema => _schema;

  StrongCerealList.from(this._inner) : _schema = _makeSchema(_inner);

  // todo: make this constructor a Result<Self, Err> factory
  StrongCerealList(this._schema, this._inner)
      : assert(_inner.iter().enumerate().every((idx, element) => _schema.match(element.getSchema(), idx)));

  static CerealListSchema _makeSchema(List<CerealValue> values) {
    if (values.isEmpty) {
      return const WeakListSchema(0); // we don't know what can go into this so, default to List<dynamic> basically
    }

    final type = values[0].getSchema();
    assert(values.every((element) => element.getSchema() == type));

    return StrongListSchema(type);
  }

  @override
  Type get concreteType => StrongCerealList;

  Option<CerealSchema> getElementType() => value.iter().next().map((val) => val.getSchema());

  @override
  CerealSchema getSchema() => _schema;

  @override
  CerealValue operator [](int index) {
    return _inner[index];
  }

  @override
  int get length => _inner.length;

  @override
  set length(int newLength) => _inner.length = newLength;

  @override
  void operator []=(int index, CerealValue value) {
    if (_schema is WeakListSchema) {
      _schema = StrongListSchema(value.getSchema());
    } else if ((_schema as StrongListSchema).type != value.getSchema()) {
      throw ArgumentError(
          "The type ${value.concreteType} is not assignable to ${(_schema as StrongListSchema).type}", "value");
    }
    _inner[index] = value;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is StrongCerealList &&
          runtimeType == other.runtimeType &&
          _schema == other._schema &&
          _listEq(_inner, other._inner) ||
      other is BasicCerealList && _listEq(value, other.items);

  @override
  int get hashCode => _schema.hashCode ^ _inner.hashCode;
}
