part of libmolk.cereal.value;

class CerealTuple with CerealValue {
  List<CerealValue> _inner;

  UnmodifiableListView<CerealValue> get value => UnmodifiableListView(_inner);

  CerealTuple.from(this._inner);

  CerealTuple(this._inner);

  List<CerealSchema> getElementTypes() {
    return _inner.iter().map((val) => val.getSchema()).collect(ListCollector());
  }

  @override
  TupleSchema getSchema() => TupleSchema(getElementTypes());

  @override
  Type get concreteType => CerealTuple;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CerealTuple && runtimeType == other.runtimeType && _listEq(_inner, other._inner);

  @override
  int get hashCode => _inner.hashCode;
}
