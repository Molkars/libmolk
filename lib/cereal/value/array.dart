part of 'lib.dart';

class CerealArray with CerealValue {
  List<CerealValue> _inner;

  UnmodifiableListView<CerealValue> get value => UnmodifiableListView(_inner);

  CerealArray(this._inner);
  CerealArray.from(Iterable<CerealValue> value)
      : _inner = value.toList(growable: false);

  Option<CerealSchema> getElementType() =>
      _inner.isNotEmpty.then(() => _inner[0].getSchema());

  @override
  CerealSchema getSchema() => ArraySchema.raw(getElementType(), _inner.length);

  @override
  Type get concreteType => CerealArray;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is CerealArray && runtimeType == other.runtimeType && _inner == other._inner;

  @override
  int get hashCode => _inner.hashCode;
}
