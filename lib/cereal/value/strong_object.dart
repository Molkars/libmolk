part of 'lib.dart';

class StrongCerealObject with CerealValue, MapMixin<String, CerealValue> {
  final StrongObjectSchema schema;
  final Map<String, CerealValue> _inner;

  static final StrongCerealObject empty = StrongCerealObject(StrongObjectSchema.empty, {});

  UnmodifiableMapView<String, CerealValue> get value => UnmodifiableMapView(_inner);

  StrongCerealObject.from(this._inner) : schema = _schema(_inner);

  StrongCerealObject.clone(StrongCerealObject other)
      : schema = other.schema,
        _inner = Map.of(other._inner);

  StrongCerealObject(this.schema, this._inner)
      : assert(_setEq(_inner.keys.toSet(), schema.fields.keys.toSet()) &&
            schema.fields.iter().every((key, schema) => _inner[key]!.getSchema() == schema));

  static StrongObjectSchema _schema(Map<String, CerealValue> values) =>
      StrongObjectSchema(values.map((key, value) => MapEntry(key, value.getSchema())));

  @override
  Type get concreteType => StrongCerealObject;

  @override
  StrongObjectSchema getSchema() => schema;

  @override
  CerealValue? operator [](Object? key) => _inner[key];

  @override
  void operator []=(String key, CerealValue value) {
    if (!schema.fields.containsKey(key)) throw ArgumentError("No field found for $key in strong-object.");
    if (schema.fields[key]! != value.getSchema()) {
      throw ArgumentError("Type ${value.getSchema()} is not assignable to ${schema.fields[key]!}");
    }
    _inner[key] = value;
  }

  @override
  void clear() => throw UnsupportedError("Cannot clear strong-object");

  @override
  Iterable<String> get keys => _inner.keys;

  @override
  CerealValue? remove(Object? key) => throw UnsupportedError("Cannot remove key from strong-object");

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is StrongCerealObject &&
          runtimeType == other.runtimeType &&
          schema == other.schema &&
          _mapEq(_inner, other._inner) ||
      other is BasicCerealObject && _mapEq(value, other.items);

  @override
  int get hashCode => schema.hashCode ^ _inner.hashCode;
}
