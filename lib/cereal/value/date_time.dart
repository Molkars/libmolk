part of 'lib.dart';

class CerealDateTime with CerealValue {
  final DateTime value;

  const CerealDateTime(this.value);

  @override
  Type get concreteType => CerealDateTime;

  @override
  DateTimeSchema getSchema() => const DateTimeSchema();

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is CerealDateTime && runtimeType == other.runtimeType && value == other.value;

  @override
  int get hashCode => value.hashCode;
}