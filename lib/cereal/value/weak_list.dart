part of 'lib.dart';

class WeakCerealList with CerealValue {
  final List<CerealValue> _inner;

  UnmodifiableListView<CerealValue> get value => UnmodifiableListView(_inner);

  const WeakCerealList(this._inner);

  @override
  Type get concreteType => WeakCerealList;

  List<CerealSchema> getElementSchema() => _inner.map((v) => v.getSchema()).toList();

  @override
  WeakListSchema getSchema() => WeakListSchema(_inner.length);

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is WeakCerealList && runtimeType == other.runtimeType && _listEq(_inner, other._inner);

  @override
  int get hashCode => _inner.hashCode;
}
