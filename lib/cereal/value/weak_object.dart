part of 'lib.dart';

class WeakCerealObject with CerealValue {
  final Map<String, CerealValue> _inner;

  UnmodifiableMapView<String, CerealValue> get value => UnmodifiableMapView(_inner);

  const WeakCerealObject(this._inner);

  Map<String, CerealSchema> getEntrySchema() => _inner.map((key, value) => MapEntry(key, value.getSchema()));

  @override
  WeakObjectSchema getSchema() => const WeakObjectSchema();

  @override
  Type get concreteType => WeakCerealObject;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is WeakCerealObject && runtimeType == other.runtimeType && _mapEq(_inner, other._inner);

  @override
  int get hashCode => _inner.hashCode;
}
