part of libmolk.cereal.value;

class CerealOption with CerealValue {
  Option<CerealValue> inner;

  CerealOption(CerealValue value) : inner = Option.some(value);
  CerealOption.none() : inner = Option.none();
  CerealOption.from(this.inner);

  @override
  Type get concreteType => CerealOption;

  Option<CerealSchema> getElementType() => inner.map((val) => val.getSchema());

  @override
  OptionSchema getSchema() => OptionSchema(getElementType());

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is CerealOption && runtimeType == other.runtimeType && inner == other.inner;

  @override
  int get hashCode => inner.hashCode;
}
