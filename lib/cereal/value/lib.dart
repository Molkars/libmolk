library libmolk.cereal.value;

import 'dart:collection';

import 'package:libmolk_dart/base/clone.dart';
import 'package:libmolk_dart/cereal/lib.dart';
import 'package:libmolk_dart/iter/iter.dart';
import 'package:libmolk_dart/iter/pair_iter.dart';

part 'array.dart';
part 'date_time.dart';
part 'option.dart';
part 'strong_list.dart';
part 'strong_object.dart';
part 'tuple.dart';
part 'weak_list.dart';
part 'weak_object.dart';

mixin CerealValue {
  CerealSchema getSchema();

  Type get concreteType;
}

bool _mapEq<K, V>(Map<K, V> a, Map<K, V> b) => a.keys.toSet() == b.keys.toSet() && a.keys.every((k) => a[k]! == b[k]!);
bool _listEq<E>(List<E> a, List<E> b) => a.length == b.length && a.iter().zip(b.iter()).every((a, b) => a == b);
bool _setEq<E>(Set<E> a, Set<E> b) => a.length == b.length && a.every(b.contains);