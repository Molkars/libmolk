import 'package:libmolk_dart/base/pair.dart';

import '../base/option.dart';
import 'iter.dart' as iter;

export 'package:libmolk_dart/base/pair.dart';

mixin PairIter<K, V> {
  Iterator<Pair<K, V>> iterator() => _DartIterator<K, V>(this);

  Iterable<Pair<K, V>> iterable() => _DartIterable<K, V>(this);

  void forEach(void Function(K key, V value) f) {
    for (final pair in iterable()) {
      f(pair.key, pair.value);
    }
  }

  MapPair<K, V, T> map<T>(T Function(K key, V value) f) => MapPair(this, f);

  InnerMapPair<K, V, K2, V2> mapPair<K2, V2>(Pair<K2, V2> Function(K key, V value) f) => InnerMapPair(this, f);

  FilterPair<K, V> filter(bool Function(K key, V value) f) => FilterPair(this, f);

  ZipPair<K, V, K2, V2> zip<K2, V2>(PairIter<K2, V2> other) => ZipPair(this, other);

  EnumeratePair<K, V> enumerate() => EnumeratePair(this);

  bool every(bool Function(K key, V value) f) {
    Option<Pair<K, V>> elem;
    bool _filter(Pair<K, V> pair) => f(pair.key, pair.value);
    while ((elem = next()).filter(_filter).isSome) {}
    return elem.isNone;
  }

  bool any(bool Function(K key, V value) f) {
    Option<Pair<K, V>> elem;
    bool _filter(Pair<K, V> pair) => f(pair.key, pair.value);
    while ((elem = next()).isSome && elem.filter(_filter).isNone) {}
    return elem.isSome;
  }

  Option<Pair<K, V>> find(bool Function(K key, V val) f) {
    Option<Pair<K, V>> elem;
    bool _filter(Pair<K, V> pair) => f(pair.key, pair.value);
    while ((elem = next()).isSome && elem.filter(_filter).isNone) {}
    return elem;
  }

  Option<int> sizeHint() => Option.none();

  Option<int> positionHint() => Option.none();

  Option<Pair<K, V>> next();

  O collect<O>(PairCollector<K, V, O> collector) => collector.collect(this);

  void ignore() => collect(const _CollectNone());
}

class _CollectNone<K, V> with PairCollector<K, V, void> {
  const _CollectNone();

  @override
  void collect(PairIter<K, V> iter) {
    for (var _ in iter.iterable()) {}
  }
}

class _DartIterator<K, V> implements Iterator<Pair<K, V>> {
  final PairIter<K, V> _iter;
  Option<Pair<K, V>> _elem = Option.none();

  _DartIterator(this._iter);

  @override
  Pair<K, V> get current => _elem.unwrap();

  @override
  bool moveNext() {
    _elem = _iter.next();
    return _elem.isSome;
  }
}

class _DartIterable<K, V> extends Iterable<Pair<K, V>> {
  final PairIter<K, V> _iter;

  _DartIterable(this._iter);

  @override
  Iterator<Pair<K, V>> get iterator => _DartIterator(_iter);
}

mixin PairCollector<K, V, O> {
  O collect(PairIter<K, V> iter);
}

class ForEachPair<K, V> with iter.Iter<void> {
  final PairIter<K, V> _iter;
  final void Function(K key, V value) _f;

  ForEachPair(this._iter, this._f);

  @override
  Option<int> sizeHint() => _iter.sizeHint();

  @override
  Option<int> positionHint() => _iter.positionHint();

  @override
  Option<void> next() => _iter.next().map((p) => _f(p.key, p.value));
}

class MapPair<K, V, T> with iter.Iter<T> {
  final PairIter<K, V> _iter;
  final T Function(K, V) _f;

  MapPair(this._iter, this._f);

  @override
  Option<int> sizeHint() => _iter.sizeHint();

  @override
  Option<int> positionHint() => _iter.positionHint();

  @override
  Option<T> next() => _iter.next().map((p) => _f(p.key, p.value));
}

class InnerMapPair<K, V, K2, V2> with PairIter<K2, V2> {
  final Pair<K2, V2> Function(K key, V value) _f;
  final PairIter<K, V> _iter;

  InnerMapPair(this._iter, this._f);

  @override
  Option<int> sizeHint() => _iter.sizeHint();

  @override
  Option<int> positionHint() => _iter.positionHint();

  @override
  Option<Pair<K2, V2>> next() => _iter.next().map((val) => _f(val.key, val.value));
}

class FilterPair<K, V> with PairIter<K, V> {
  final PairIter<K, V> _iter;
  final bool Function(K key, V value) _f;

  FilterPair(this._iter, this._f);

  @override
  Option<Pair<K, V>> next() {
    Option<Pair<K, V>> elem;
    bool _filter(Pair<K, V> pair) => _f(pair.key, pair.value);
    while ((elem = _iter.next()).isSome && elem.filter(_filter).isSome) {}
    return elem;
  }
}

class ZipPair<K1, V1, K2, V2> with PairIter<Pair<K1, V1>, Pair<K2, V2>> {
  final PairIter<K1, V1> _a;
  final PairIter<K2, V2> _b;

  ZipPair(this._a, this._b);

  @override
  Option<int> sizeHint() => _a.sizeHint().zipWith((_) => _b.sizeHint()).map((val) => val.left + val.right);

  @override
  Option<int> positionHint() => _a.positionHint().zipWith((_) => _b.positionHint()).map((val) => val.left + val.right);

  @override
  Option<Pair<Pair<K1, V1>, Pair<K2, V2>>> next() {
    return _a.next().zipWith((val) => _b.next());
  }
}

class EnumeratePair<K, V> with PairIter<int, Pair<K, V>> {
  final PairIter<K, V> _iter;
  int _index = 0;

  EnumeratePair(this._iter);

  @override
  Option<Pair<int, Pair<K, V>>> next() => _iter.next().map((val) => pair(_index++, val));
}

class DartMapIter<K, V> with PairIter<K, V> {
  final Iterator<MapEntry<K, V>> _iterator;

  DartMapIter(this._iterator);

  @override
  Option<Pair<K, V>> next() {
    if (_iterator.moveNext()) {
      final next = _iterator.current;
      return Option.some(Pair(next.key, next.value));
    }
    return Option.none();
  }
}

class PairMapCollector<K, V> with PairCollector<K, V, Map<K, V>> {
  @override
  Map<K, V> collect(PairIter<K, V> iter) {
    var out = <K, V>{};
    iter.forEach((key, value) => out[key] = value);
    return out;
  }
}

extension MapToPairIter<K, V> on Map<K, V> {
  DartMapIter<K, V> iter() => DartMapIter(entries.iterator);
}

class IterPairIter<K, V> with PairIter<K, V> {
  final iter.Iter<Pair<K, V>> _iter;

  IterPairIter(this._iter);

  @override
  Option<Pair<K, V>> next() {
    return _iter.next();
  }
}

extension IterToPairIter<K, V> on iter.Iter<Pair<K, V>> {
  PairIter<K, V> toPairIter() {
    return IterPairIter(this);
  }
}
