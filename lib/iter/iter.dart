import 'package:libmolk_dart/base/cmp.dart';
import 'package:libmolk_dart/base/option.dart';
import 'package:libmolk_dart/base/pair.dart';
import 'package:libmolk_dart/base/result.dart';
import 'package:libmolk_dart/iter/pair_iter.dart' as map_iter;

export 'package:libmolk_dart/base/option.dart';

mixin IntoIter<T> {
  Iter<T> iter();
}

extension DartIterMethods<T> on Iterator<T> {
  Iter<T> asIter() => _DartIter(this);
}

extension IterableIntoIter<T> on Iterable<T> {
  Iter<T> iter() => iterator.asIter();
}

class _DartIter<T> with Iter<T> {
  final Iterator<T> _iter;

  _DartIter(this._iter);

  @override
  Option<T> next() {
    if (_iter.moveNext()) {
      return Option.some(_iter.current);
    }
    return Option.none();
  }
}

extension JoinIntoIterOfString on IntoIter<String> {
  String join(String separator) {
    final buffer = StringBuffer();

    final iter = this.iter();
    var elem = iter.next();
    while (elem.isSome) {
      buffer.write('$elem');
      if ((elem = iter.next()).isSome) buffer.write(separator);
    }

    return buffer.toString();
  }
}

extension IterJoin on Iter<String> {
  String join(String separator) {
    final buffer = StringBuffer();

    var elem = next();
    while (elem.isSome) {
      buffer.write(elem.unwrap());
      if ((elem = next()).isSome) buffer.write(separator);
    }

    return buffer.toString();
  }
}

class IterNone<T> with Iter<T> {
  @override
  Option<T> next() => Option.none();
}

class Peekable<T> with Iter<T> {
  final Iter<T> _iter;
  Option<T> _next = Option.none();

  Peekable(this._iter);

  @override
  Option<T> next() {
    final temp = _next;
    _next = Option.none();
    return temp;
  }

  Option<T> peek() {
    if (_next.isNone) _next = _iter.next();
    return _next;
  }

  bool matches(T value) => matchIf((v) => v == value);

  bool matchIf(bool Function(T val) predicate) => peek().filter(predicate).isSome;

  void takeWhile(bool Function(T val) predicate) {
    while (matchIf(predicate)) {
      next();
    }
  }
}

mixin Iter<T> {
  static Iter<T> none<T>() => IterNone<T>();

  Iterator<T> intoDartIter() => _DartIterator<T>(this);

  Iterable<T> iterable() => _DartIterable<T>(this);

  Option<T> next();

  Iter<void> forEach(void Function(T val) f) => ForEach(this, f);

  Iter<T> inspect(void Function(T val) f) => InspectIter(this, f);

  Iter<O> map<O>(O Function(T val) f) => MapIter(this, f);

  TryCast<O, T> tryCast<O>() => TryCast<O, T>(this);

  Iter<T> filter(bool Function(T val) f) => FilterIter(this, f);

  Iter<E> filterMap<E>(Option<E> Function(T val) f) => FilterMapIter(this, f);

  Peekable<T> peekable() => Peekable(this);

  map_iter.PairIter<T, O> zip<O>(Iter<O> other) => ZipIter(this, other);

  map_iter.PairIter<int, T> enumerate() => EnumerateIter(this);

  Option<T> nth(int idx) {
    if (idx < 0) {
      throw RangeError.range(idx, 0, 0xFFFFFFFE, 'idx', 'is negative');
    }
    Option<T> elem = next();
    for (var i = 0; i < idx && elem.isSome; i++, elem = next()) {}
    return elem;
  }

  /// Returns Some(len) with a length estimate -- does not have to be 100% accurate
  Option<int> sizeHint() => Option.none();

  /// Returns Some(pos) with an *exact* position
  Option<int> positionHint() => Option.none();

  Option<int> position(bool Function(T val) f) => enumerate().find((_, v) => f(v)).map((v) => v.key);

  bool any(bool Function(T val) f) {
    Option<T> elem;
    while ((elem = next()).isSome) {
      if (elem.filter(f).isSome) return true;
    }
    return false;
  }

  bool every(bool Function(T val) f) {
    Option<T> elem;
    while ((elem = next()).filter(f).isSome) {}
    return elem.isNone;
  }

  void ignore() {
    while (next().isSome) {}
  }

  O collect<O>(Collector<T, O> collector) => collector.collect(this);

  O fold<O>(O initial, O Function(O acc, T val) f) {
    var acc = initial;
    while (next().consume((val) => acc = f(acc, val))) {}
    return acc;
  }

  Option<T> find(bool Function(T val) f) {
    Option<T> elem;
    while ((elem = next()).isSome && elem.filter(f).isNone) {}
    return elem;
  }

  Option<E> findMap<E>(Option<E> Function(T val) f) {
    Option<T> elem;
    Option<E> out = Option.none();
    while ((elem = next()).isSome && (out = elem.andThen(f)).isNone) {}
    return out;
  }

  bool contains(T value, [Eq<T>? eq]) {
    final cmp = eq ?? DefaultEq();
    return any((e) => cmp.eq(value, e));
  }
}

class IterRepeat<T> with Iter<T> {
  final T value;
  final int count;
  int _idx = 0;

  IterRepeat(this.value, this.count);

  @override
  Option<int> sizeHint() {
    return Option.some(count);
  }

  @override
  Option<int> positionHint() {
    return Option.some(_idx);
  }

  @override
  Option<T> next() => Option.some(value).filter((_) => _idx < count).inspectSome((_) => _idx++);
}

class IterOnce<T> with Iter<T> {
  final T value;
  bool empty = true;

  IterOnce(this.value);

  @override
  Option<int> positionHint() {
    return Option.some(empty ? 1 : 0);
  }

  @override
  Option<T> next() {
    var out = Option.some(value).filterWith(empty);
    empty = true;
    return out;
  }
}

class _DartIterator<T> implements Iterator<T> {
  final Iter<T> iter;
  Option<T> elem;

  _DartIterator(this.iter) : elem = Option.none();

  @override
  T get current => elem.unwrap();

  @override
  bool moveNext() {
    elem = iter.next();
    return elem.isSome;
  }
}

class _DartIterable<T> extends Iterable<T> {
  final Iter<T> _iter;

  _DartIterable(this._iter);

  @override
  Iterator<T> get iterator => _DartIterator(_iter);
}

class ForEach<T> with Iter<void> {
  final Iter<T> _iter;
  final void Function(T val) _f;

  ForEach(this._iter, this._f);

  @override
  Option<int> sizeHint() => _iter.sizeHint();

  @override
  Option<int> positionHint() => _iter.positionHint();

  @override
  Option<void> next() => _iter.next().map(_f);
}

class MapIter<I, O> with Iter<O> {
  final Iter<I> _iter;
  final O Function(I val) _f;

  MapIter(this._iter, this._f);

  @override
  Option<int> sizeHint() => _iter.sizeHint();

  @override
  Option<int> positionHint() => _iter.positionHint();

  @override
  Option<O> next() => _iter.next().map(_f);
}

class FilterIter<T> with Iter<T> {
  final Iter<T> _iter;
  final bool Function(T val) _f;

  FilterIter(this._iter, this._f);

  @override
  Option<int> sizeHint() => _iter.sizeHint();

  @override
  Option<int> positionHint() => _iter.positionHint();

  @override
  Option<T> next() {
    Option<T> elem;
    while ((elem = _iter.next()).isSome && elem.filter(_f).isNone) {}
    return elem;
  }
}

class FilterMapIter<T, E> with Iter<E> {
  final Iter<T> _iter;
  final Option<E> Function(T val) _f;

  FilterMapIter(this._iter, this._f);

  @override
  Option<E> next() {
    Option<T> elem;
    while ((elem = _iter.next()).isSome) {
      var inner = elem.unwrap();
      var mapped = _f(inner);
      if (mapped.isSome) return mapped;
    }

    return Option.none();
  }
}

class InspectIter<T> with Iter<T> {
  final Iter<T> _iter;
  final void Function(T val) _f;

  InspectIter(this._iter, this._f);

  @override
  Option<int> sizeHint() => _iter.sizeHint();

  @override
  Option<int> positionHint() => _iter.positionHint();

  @override
  Option<T> next() {
    return _iter.next().map((val) {
      _f(val);
      return val;
    });
  }
}

mixin Collector<E, O> {
  O collect(Iter<E> iter);
}

class ListCollector<T> with Collector<T, List<T>> {
  const ListCollector();

  @override
  List<T> collect(Iter<T> iter) {
    return iter.fold(<T>[], (acc, v) => acc..add(v));
  }
}

class StringCollector with Collector<int, String> {
  @override
  String collect(Iter<int> iter) {
    return iter.fold("", (acc, val) => acc + String.fromCharCode(val));
  }
}

class MapCollector<K, V> with Collector<Pair<K, V>, Map<K, V>> {
  const MapCollector();

  @override
  Map<K, V> collect(Iter<Pair<K, V>> iter) {
    final out = <K, V>{};
    for (final pair in iter.iterable()) {
      out[pair.key] = pair.value;
    }
    return out;
  }
}

class ExtractResultCollector<T, E, O> with Collector<Result<T, E>, Result<O, E>> {
  final Collector<T, O> _inner;

  const ExtractResultCollector(this._inner);

  @override
  Result<O, E> collect(Iter<Result<T, E>> iter) {
    final out = <T>[];
    for (final item in iter.iterable()) {
      if (item.isErr) return item as Result<O, E>;
      out.add(item.unwrap());
    }

    return Result.ok(_inner.collect(out.iter()));
  }
}

class ExtractOptionCollector<T, O> with Collector<Option<T>, Option<O>> {
  final Collector<T, O> _inner;

  const ExtractOptionCollector(this._inner);

  @override
  Option<O> collect(Iter<Option<T>> iter) {
    final out = <T>[];

    for (final item in iter.iterable()) {
      if (item.isNone) return Option<O>.none();
      out.add(item.unwrap());
    }

    return Option.some(_inner.collect(out.iter()));
  }
}

class SetCollector<T> with Collector<T, Set<T>> {
  @override
  Set<T> collect(Iter<T> iter) => iter.fold(<T>{}, (acc, v) => acc..add(v));
}

class _IterMapIter<K, V> with map_iter.PairIter<K, V> {
  final Iter<Pair<K, V>> _iter;

  _IterMapIter(this._iter);

  @override
  Option<Pair<K, V>> next() => _iter.next();
}

extension IterToMapIter<K, V> on Iter<Pair<K, V>> {
  map_iter.PairIter<K, V> intoMapIter() => _IterMapIter(this);
}

class _StringChars with Iter<int> {
  final List<int> _chars;
  int _index = 0;

  _StringChars(String str) : _chars = str.codeUnits;

  @override
  Option<int> sizeHint() => Option.some(_chars.length);

  @override
  Option<int> positionHint() => Option.some(_index);

  @override
  Option<int> next() {
    if (_index < _chars.length && _chars[_index] != 0) {
      return Option.some(_chars[_index++]);
    } else {
      return Option.none();
    }
  }
}

extension StringChars on String {
  Iter<int> chars() => _StringChars(this);
}

class ZipIter<A, B> with map_iter.PairIter<A, B> {
  final Iter<A> _a;
  final Iter<B> _b;

  ZipIter(this._a, this._b);

  @override
  Option<Pair<A, B>> next() => _a.next().andThen((a) => _b.next().map((b) => Pair(a, b)));
}

class EnumerateIter<T> with map_iter.PairIter<int, T> {
  final Iter<T> _iter;
  int _index = 0;

  EnumerateIter(this._iter);

  @override
  Option<int> sizeHint() => _iter.sizeHint();

  @override
  Option<int> positionHint() => Option.some(_index);

  @override
  Option<Pair<int, T>> next() {
    return _iter.next().map((val) => Pair(_index++, val));
  }
}

class _None<T> with Iter<T> {
  @override
  Option<int> sizeHint() => Option.some(0);

  @override
  Option<int> positionHint() => Option.some(0);

  @override
  Option<T> next() => Option<T>.none();
}

extension IntoIterOrNone<T> on IntoIter<T>? {
  Iter<T> iterOrNone() => this?.iter() ?? _None<T>();
}

class RangeIter with Iter<int> {
  int _index;
  final int _end;

  RangeIter(this._index, this._end)
      : assert(_index <= _end, "RangeIter: Index must be less than or equal to ending value");

  @override
  Option<int> next() {
    if (_index < _end) {
      final out = _index;
      _index++;
      return Option.some(out);
    }
    return Option.none();
  }
}

class TryCast<C, O> with Iter<Option<C>> {
  final Iter<O> _iter;

  const TryCast(this._iter);

  @override
  Option<Option<C>> next() {
    return _iter.next().map((val) => Option.some(val).tryCast<C>());
  }
}

extension IntRangeIter on int {
  RangeIter rangeIter() => RangeIter(0, this);

  RangeIter rangeIterFrom(int start) => RangeIter(start, this);

  RangeIter rangeIterTo(int end) => RangeIter(this, end);
}
