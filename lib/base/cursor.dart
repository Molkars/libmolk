import 'dart:collection';
import 'dart:math';

import 'package:libmolk_dart/iter/iter.dart';

class Cursor with Iter<int> {
  final UnmodifiableListView<int> _bytes;
  int _index = 0;

  Cursor(Iterable<int> bytes) : _bytes = UnmodifiableListView(bytes);

  @override
  Option<int> next() {
    Option<int> out = Option.none();
    if (_index < _bytes.length) {
      out = Option.some(_bytes[_index]);
      _index++;
    }
    return out;
  }

  Option<int> peek([int n = 0]) => Option.someIfNotNull(_bytes.elementAt(_index + n));

  /// Take a char matching [char]
  bool take(int char) => next().filter((val) => val == char).isSome;
  
  Option<bool> maybeTake(int char) => next().map((val) => val == char);

  /// Match the next char matching [char]
  bool match(int char) => peek().filter((val) => val == char).isSome;

  List<int> takeWhile(bool Function(int char) f) {
    var out = <int>[];
    for (int i = _index; i < _bytes.length && f(_bytes[i]); i++) {
      out.add(_bytes[i]);
    }
    return out;
  }

  Option<List<int>> sliceForward(int length) {
    return (_index + length < _bytes.length).then(() => _bytes.sublist(_index, _index + length));
  }

  void skip(int n) => _index = min(_index + n, _bytes.length);

  bool doTake(int char) {
    if (match(char)) {
      _index++;
      return true;
    }
    return false;
  }

  String debug() => "Cursor{position: $_index}";
}

