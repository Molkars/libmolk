class Ascii {
  const Ascii._();

  static const int nul = 0;
  static const int soh = 1;
  static const int stx = 2;
  static const int etx = 3;
  static const int eot = 4;
  static const int enq = 5;
  static const int ack = 6;
  static const int bel = 7;
  static const int bs = 8;
  static const int tab = 9;
  static const int lf = 10;
  static const int vt = 11;
  static const int ff = 12;
  static const int cr = 13;
  static const int so = 14;
  static const int si = 15;
  static const int dle = 16;
  static const int dcl = 17;
  static const int dc2 = 18;
  static const int dc3 = 19;
  static const int dc4 = 20;
  static const int nak = 21;
  static const int syn = 22;
  static const int etb = 23;
  static const int can = 24;
  static const int em = 25;
  static const int sub = 26;
  static const int esc = 27;
  static const int fs = 28;
  static const int gs = 29;
  static const int rs = 30;
  static const int us = 31;
  static const int space = 32;
  static const int bang = 33;
  static const int quotation = 34;
  static const int pound = 35;
  static const int dollar = 36;
  static const int percent = 37;
  static const int ampersand = 38;
  static const int apostrophe = 39;
  static const int leftParent = 40;
  static const int rightParen = 41;
  static const int star = 42;
  static const int plus = 43;
  static const int comma = 44;
  static const int dash = 45;
  static const int period = 46;
  static const int slash = 47;
  static const int zero = 48;
  static const int one = 49;
  static const int two = 50;
  static const int three = 51;
  static const int four = 52;
  static const int five = 53;
  static const int six = 54;
  static const int seven = 55;
  static const int eight = 56;
  static const int nine = 57;
  static const int colon = 58;
  static const int semicolon = 59;
  static const int lessThan = 60;
  static const int equal = 61;
  static const int greaterThan = 62;
  static const int question = 63;
  static const int at = 64;
  static const int capitalA = 65;
  static const int capitalB = 66;
  static const int capitalC = 67;
  static const int capitalD = 68;
  static const int capitalE = 69;
  static const int capitalF = 70;
  static const int capitalG = 71;
  static const int capitalH = 72;
  static const int capitalI = 73;
  static const int capitalJ = 74;
  static const int capitalK = 75;
  static const int capitalL = 76;
  static const int capitalM = 77;
  static const int capitalN = 78;
  static const int capitalO = 79;
  static const int capitalP = 80;
  static const int capitalQ = 81;
  static const int capitalR = 82;
  static const int capitalS = 83;
  static const int capitalT = 84;
  static const int capitalU = 85;
  static const int capitalV = 86;
  static const int capitalW = 87;
  static const int capitalX = 88;
  static const int capitalY = 89;
  static const int capitalZ = 90;
  static const int leftBracket = 91;
  static const int backslash = 92;
  static const int rightBracket = 93;
  static const int caret = 94;
  static const int underscore = 95;
  static const int backtick = 96;
  static const int letterA = 97;
  static const int letterB = 98;
  static const int letterC = 99;
  static const int letterD = 100;
  static const int letterE = 101;
  static const int letterF = 102;
  static const int letterG = 103;
  static const int letterH = 104;
  static const int letterI = 105;
  static const int letterJ = 106;
  static const int letterK = 107;
  static const int letterL = 108;
  static const int letterM = 109;
  static const int letterN = 110;
  static const int letterO = 111;
  static const int letterP = 112;
  static const int letterQ = 113;
  static const int letterR = 114;
  static const int letterS = 115;
  static const int letterT = 116;
  static const int letterU = 117;
  static const int letterV = 118;
  static const int letterW = 119;
  static const int letterX = 120;
  static const int letterY = 121;
  static const int letterZ = 122;
  static const int leftBrace = 123;
  static const int horizontalBar = 124;
  static const int rightBrace = 125;
  static const int tilda = 126;
  static const int del = 127;

  static const hexadecimal = [
    letterA, letterB, letterC, letterD, letterE, letterF,
    capitalA, capitalB, capitalC, capitalD, capitalE, capitalF,
    ...numeric,
  ];

  static const numeric = [zero, one, two, three, four, five, six, seven, eight, nine];

  static bool isNumeric(int char) => char > zero - 1 && char < nine + 1;

  static bool isLetter(int char) => char > letterA - 1 && char < letterZ + 1;

  static bool isCapital(int char) => char > capitalA - 1 && char < capitalZ + 1;

  static bool isAlpha(int char) => isLetter(char) || isCapital(char);

  static bool isAlphanumeric(int char) => isAlpha(char) || isNumeric(char);

  static bool isHexadecimal(int char) =>
      isNumeric(char) || char > letterA - 1 && char < letterG || char > capitalA - 1 && char < capitalG;

  static bool isControl(int char) => char < space || char > del - 1;

  static bool isBinary(int char) => char == zero || char == one;

  static bool isWhitespace(int char) => char == tab || char == lf || char == ff || char == cr || char == space;

  static bool invalid(int char) {
    assert(char > -1);
    return char > del;
  }
}
