import 'package:libmolk_dart/base/pair.dart';
import 'package:libmolk_dart/base/ptr.dart';
import 'package:libmolk_dart/base/result.dart';

Option<T> some<T>(T value) => Option.some(value);

Option<T> none<T>() => Option<T>.none();

final class Unit {
  const Unit();
}

const Unit unit = Unit();

sealed class Option<T> {
  bool get isSome;

  bool get isNone => !isSome;

  const Option();

  const factory Option.none() = None<T>;

  const factory Option.some(T value) = Some<T>;

  factory Option.someIfNotNull(T? value) {
    if (value == null) {
      return None();
    }
    return Some(value);
  }

  Option<U> map<U>(U Function(T val) f);

  Option<U> andThen<U>(Option<U> Function(T val) f);

  Option<T> and(Option<T> other);

  Option<T> or(Option<T> other);

  Option<T> orElse(Option<T> Function() f);

  Option<T> filter(bool Function(T val) f);

  Option<T> filterWith(bool cond);

  Option<T> eq(T other);

  Option<T> ne(T other);

  bool consume(void Function(T val) f);

  Option<T> inspectSome(void Function(T val) f);

  Option<T> inspectNone(void Function() f);

  Option<T> inspect(void Function(Option<T> opt) f) {
    f(this);
    return this;
  }

  Option<Pair<T, E>> zip<E>(Option<E> other);

  Option<Pair<T, E>> zipWith<E>(Option<E> Function(T val) other);

  bool test(bool Function(T val) f);

  T unwrap();

  T unwrapOr(T defaultValue);

  T? unwrapOrNull();

  T unwrapOrElse(T Function() defaultValue);

  T expect(String message);

  Option<C> cast<C>();

  Option<C> tryCast<C>();

  void expectNone(String s);

  T call(TriOption tri) => tri(this);

  Result<T, E> okOr<E>(E err);

  Result<T, E> okOrElse<E>(E Function() err);

  static Option<T> block<T>(Option<T> Function(TriOption tri) f) {
    try {
      return f(TriOption._());
    } catch (e) {
      return Option<T>.none();
    }
  }

  static Future<Option<T>> asyncBlock<T>(Future<Option<T>> Function(TriOption tri) f) {
    try {
      return f(TriOption._());
    } catch (e) {
      return Future.value(Option<T>.none());
    }
  }
}

final class Some<T> extends Option<T> {
  final T value;

  @override
  bool get isSome => true;

  const Some(this.value);

  @override
  Option<T> and(Option<T> other) {
    return other;
  }

  @override
  Option<U> andThen<U>(Option<U> Function(T val) f) {
    return f(value);
  }

  @override
  Option<C> cast<C>() => Some(value as C);

  @override
  bool consume(void Function(T val) f) {
    f(value);
    return true;
  }

  @override
  Option<T> eq(T other) {
    return value == other ? Some(value) : None();
  }

  @override
  T expect(String message) {
    return value;
  }

  @override
  void expectNone(String s) {
    throw StateError("Expected None, got Some($value): $s");
  }

  @override
  Option<T> filter(bool Function(T val) f) {
    if (f(value)) return Some(value);
    return None();
  }

  @override
  Option<T> filterWith(bool cond) {
    if (cond) return Some(value);
    return None();
  }

  @override
  Option<T> inspectSome(void Function(T val) f) {
    f(value);
    return this;
  }

  @override
  Option<T> inspectNone(void Function() f) {
    return this;
  }

  @override
  Option<U> map<U>(U Function(T val) f) {
    return Some(f(value));
  }

  @override
  Option<T> ne(T other) {
    return value != other ? Some(value) : None();
  }

  @override
  Result<T, E> okOr<E>(E err) {
    return Ok(value);
  }

  @override
  Result<T, E> okOrElse<E>(E Function() err) {
    return Ok(value);
  }

  @override
  Option<T> or(Option<T> other) {
    return this;
  }

  @override
  Option<T> orElse(Option<T> Function() f) {
    return this;
  }

  @override
  bool test(bool Function(T val) f) {
    return f(value);
  }

  @override
  Option<C> tryCast<C>() {
    if (value is C)
      return Some(value as C);
    else
      return None();
  }

  @override
  T unwrap() {
    return value;
  }

  @override
  T unwrapOr(T defaultValue) {
    return value;
  }

  @override
  T unwrapOrElse(T Function() defaultValue) {
    return value;
  }

  @override
  T? unwrapOrNull() {
    return value;
  }

  @override
  Option<Pair<T, E>> zip<E>(Option<E> other) {
    return switch (other) {
      Some(value: var other) => Some(Pair(this.value, other)),
      None() => None(),
    };
  }

  @override
  Option<Pair<T, E>> zipWith<E>(Option<E> Function(T val) other) {
    return switch (other(value)) {
      Some(value: var other) => Some(Pair(this.value, other)),
      None() => None(),
    };
  }

  @override
  String toString() => "Some($value)";

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is Some && runtimeType == other.runtimeType && value == other.value;

  @override
  int get hashCode => value.hashCode;
}

final class None<T> extends Option<T> {
  @override
  bool get isSome => false;

  const None();

  @override
  Option<T> and(Option<T> other) {
    return None();
  }

  @override
  Option<U> andThen<U>(Option<U> Function(T val) f) {
    return None();
  }

  @override
  Option<C> cast<C>() {
    return None();
  }

  @override
  bool consume(void Function(T val) f) {
    return false;
  }

  @override
  Option<T> eq(T other) {
    return None();
  }

  @override
  T expect(String message) {
    throw StateError("Expected Some, got None: $message");
  }

  @override
  void expectNone(String s) {
    // Do nothing
  }

  @override
  Option<T> filter(bool Function(T val) f) {
    return None();
  }

  @override
  Option<T> filterWith(bool cond) {
    return None();
  }

  @override
  Option<T> inspectSome(void Function(T val) f) {
    return None();
  }

  @override
  Option<T> inspectNone(void Function() f) {
    f();
    return this;
  }

  @override
  Option<U> map<U>(U Function(T val) f) {
    return None();
  }

  @override
  Option<T> ne(T other) {
    return None();
  }

  @override
  Result<T, E> okOr<E>(E err) {
    return Err(err);
  }

  @override
  Result<T, E> okOrElse<E>(E Function() err) {
    return Err(err());
  }

  @override
  Option<T> or(Option<T> other) {
    return other;
  }

  @override
  Option<T> orElse(Option<T> Function() f) {
    return f();
  }

  @override
  bool test(bool Function(T val) f) {
    return false;
  }

  @override
  Option<C> tryCast<C>() {
    return None();
  }

  @override
  T unwrap() {
    throw StateError("Called unwrap() on None");
  }

  @override
  T unwrapOr(T defaultValue) {
    return defaultValue;
  }

  @override
  T unwrapOrElse(T Function() defaultValue) {
    return defaultValue();
  }

  @override
  T? unwrapOrNull() {
    return null;
  }

  @override
  Option<Pair<T, E>> zip<E>(Option<E> other) {
    return None();
  }

  @override
  Option<Pair<T, E>> zipWith<E>(Option<E> Function(T val) other) {
    return None();
  }

  @override
  String toString() => "None";

  @override
  bool operator ==(Object other) => identical(this, other) || other is None;

  @override
  int get hashCode => 0;
}

extension FutureOption<T> on Future<Option<T>> {
  Future<Option<U>> map<U>(U Function(T val) f) => then((value) => value.map(f));

  Future<Option<U>> andThen<U>(Option<U> Function(T val) f) => then((value) => value.andThen(f));

  Future<Option<T>> or(Option<T> other) => then((value) => value.or(other));

  Future<Option<T>> filter(bool Function(T val) f) => then((value) => value.filter(f));

  Future<Option<T>> eq(T other) => then((value) => value.eq(other));

  Future<Option<T>> ne(T other) => then((value) => value.ne(other));

  Future<bool> test(bool Function(T val) f) => then((value) => value.test(f));

  Future<T> unwrap() => then((value) => value.unwrap());

  Future<T> unwrapOr(T defaultValue) => then((value) => value.unwrapOr(defaultValue));

  Future<T> unwrapOrElse(T Function() defaultValue) => then((value) => value.unwrapOrElse(defaultValue));

  Future<T> expect(String message) => then((value) => value.expect(message));

  Future<Result<T, E>> okOr<E>(E err) => then((value) => value.okOr(err));

  Future<Result<T, E>> okOrElse<E>(E Function() err) => then((value) => value.okOrElse(err));

  Future<bool> get isSome => then((value) => value.isSome);

  Future<bool> get isNone => then((value) => value.isNone);
}

extension UnwrapOptionalFuture<T> on Option<Future<T>> {
  Future<Option<T>> extractFuture() => switch (this) {
        Some(value: var future) => future.then(Some.new),
        None() => Future.value(None()),
      };
}

class TriOption {
  TriOption._();

  T call<T>(Option<T> value) => value.unwrap();
}

extension FilterNotNull<T> on Option<T?> {
  Option<T> filterNotNull() => filter((T? val) => val != null).map((T? val) => val as T);
}

extension IntoPtr<T> on Option<T> {
  Ptr<T> intoPtr() => switch (this) {
        Some(value: var value) => Ptr(value),
        None() => Ptr.none(),
      };
}

extension ThenOpt on bool {
  Option<T> then<T>(T Function() factory) => this ? Option.some(factory()) : Option<T>.none();

  Option<T> andThen<T>(Option<T> Function() factory) => this ? factory() : Option<T>.none();

  Option<T> thenSome<T>(T val) => this ? Option.some(val) : Option<T>.none();

  Option<T> thenNone<T>() => Option<T>.none();
}

extension NullOption<T> on T? {
  Option<T> someIfNotNull() => this != null ? Some(this as T) : None();
}