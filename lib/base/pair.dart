
class Pair<K, V> {
  final K key;
  final V value;

  // looks like .0
  K get left => key;

  // looks like .1
  V get right => value;

  const Pair(this.key, this.value);
}

Pair<K, V> pair<K, V>(K key, V value) => Pair(key, value);