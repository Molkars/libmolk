import 'package:libmolk_dart/base/option.dart';
import 'package:libmolk_dart/base/result.dart';

mixin Eq<T> {
  bool eq(T a, T b);

  bool ne(T a, T b) => !eq(a, b);
}

mixin Cmp<T> {
  int cmp(T a, T b);

  bool gt(T a, T b) => cmp(a, b) > 0;

  bool lt(T a, T b) => cmp(a, b) < 0;

  bool ge(T a, T b) => cmp(a, b) >= 0;

  bool le(T a, T b) => cmp(a, b) <= 0;
}

class DefaultEq<T> with Eq<T> {
  const DefaultEq();

  @override
  bool eq(T a, T b) {
    if (a is Option)
      return a.isNone && (b as Option).isNone ||
          a.isSome && (b as Option).isSome && const DefaultEq().eq(a.unwrap(), b.unwrap());
    if (a is Result)
      return a.isOk && (b as Result).isOk && const DefaultEq().eq(a.unwrap(), b.unwrap()) ||
          a.isErr && (b as Result).isErr && const DefaultEq().eq(a.unwrapErr(), b.unwrapErr());
    return identical(a, b);
  }

  @override
  bool ne(T a, T b) => !eq(a, b);
}

class ObjectEq<T> with Eq<T> {
  const ObjectEq();

  @override
  bool eq(T a, T b) => identical(a, b) || a == b;
}

class DefaultCmp<T> with Cmp<T> {
  @override
  int cmp(T a, T b) {
    if (T == int) return (a as int) - (b as int);
    if (T == double) return ((a as double) - (b as double)).ceil();
    if (T == bool) return ((a as bool) ? 1 : 0) - ((b as bool) ? 1 : 0);
    if (a is Option) {
      if (a.isSome && (b as Option).isSome) return const DefaultCmp().cmp(a.unwrap(), b.unwrap());
      if (a.isNone && (b as Option).isNone) return 0;
      return a.isSome ? 1 : -1; // if a is some, then b is none so a > b -> 1 and vice-versa -> -1
    }
    if (a is Result) {
      if (a.isOk && (b as Result).isOk) return const DefaultCmp().cmp(a.unwrap(), b.unwrap());
      if (a.isErr && (b as Result).isErr) return const DefaultCmp().cmp(a.unwrapErr(), b.unwrapErr());
      return a.isOk ? 1 : -1;
    }
    if (a is Comparable<T>) return a.compareTo(b);
    throw ArgumentError('DefaultCmp<$T>: $T is not a "fundamental" type or does not implement Comparable');
    // return 0;
  }

  const DefaultCmp();
}
