

mixin Clone<T> {
  T clone();
}