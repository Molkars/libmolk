import 'package:libmolk_dart/base/option.dart';

class Box<T> {
  T value;

  Box._(this.value);

  factory Box(T value) => Box._(value);

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is Box && value == other.value;

  @override
  int get hashCode => value.hashCode;
}

class Ptr<T> {
  Option<T> _value;

  Ptr._(this._value);

  Ptr._none() : _value = Option<T>.none();

  factory Ptr.none() => Ptr<T>._none();

  factory Ptr(T value) => Ptr._(Option.some(value));

  T? orNull() => _value.isSome ? _value.unwrap() : null;

  T deref() => _value.expect("deref: null pointer");

  bool get isNone => _value.isNone;

  bool get isSome => _value.isSome;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Ptr &&
          (isNone == other.isNone || _value.unwrap() == other._value.unwrap()));

  @override
  int get hashCode => identityHashCode(this);

  void clear() {
    _value = Option<T>.none();
  }

  void set(T value) => _value = Option.some(value);
}
