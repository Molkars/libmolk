import 'package:libmolk_dart/base/pair.dart';

import 'option.dart';

sealed class Result<V, E> {
  const Result();

  const factory Result.ok(V value) = Ok<V, E>;

  const factory Result.err(E err) = Err<V, E>;

  bool get isOk;

  bool get isErr;

  V expect(String message);

  V unwrap();

  V unwrapOr(V defaultValue);

  V unwrapOrElse(V Function(E err) defaultValue);

  E expectErr(String message);

  E unwrapErr();

  E unwrapErrOr(E defaultValue);

  E unwrapErrOrElse(E Function(V val) defaultValue);

  Result<T, E> map<T>(T Function(V val) f);

  Result<V, T> mapErr<T>(T Function(E err) f);

  Result<U, E> andThen<U>(Result<U, E> Function(V val) f);

  Result<V, E> and(Result<V, E> other);

  Result<V, E> or(Result<V, E> other);

  Result<V, E> filterOr(bool Function(V val) f, E err);

  Result<V, E> filterOrElse(bool Function(V val) f, E Function(V val) err);

  Option<V> ok();

  Option<E> err();

  Result<Pair<V, O>, E> zip<O>(Result<O, E> other);

  Result<Pair<V, O>, E> zipWith<O>(Result<O, E> Function(V val) other);

  Result<C, E> cast<C>();

  Result<V, C> castErr<C>();

  Result<C, E> tryCastOr<C>(E err);

  Result<C, E> tryCastOrElse<C>(E Function(V val) err);

  Result<V, C> tryCastErrOr<C>(C err);

  Result<V, C> tryCastErrOrElse<C>(C Function(E err) err);

  Result<V, E> inspectOk(void Function(V val) f);

  Result<V, E> inspectErr(void Function(E err) f);

  Result<V, E> inspect(void Function(Result<V, E> res) f) {
    f(this);
    return this;
  }

  V tri(TriResult tri) => tri(this);

  V call(TriResult tri) => tri(this);

  static Result<T, E> wrap<T, E>(T Function() f) {
    try {
      return Ok(f());
    } on _ResErr<E> catch (e) {
      return Err(e.err);
    } catch (e) {
      print("Result<$T, $E>#block() caught exception not of type $E. Please throw an instance of $E instead.");
      rethrow;
    }
  }

  static Future<Result<T, E>> wrapAsync<T, E>(Future<T> Function() f) => f().then((value) => Result.ok(value));

  static Result<V, E> block<V, E>(Result<V, E> Function(TriResult<E> tri) block) {
    try {
      final value = block(TriResult<E>._());
      return value;
    } on _ResErr<E> catch (e) {
      return Err(e.err);
    } catch (e) {
      print("Result<$V, $E>#block() caught exception not of type $E; Please throw an instance of $E instead.");
      rethrow;
    }
  }

  static Future<Result<V, E>> asyncBlock<V, E>(Future<Result<V, E>> Function(TriResult<E> tri) block) async {
    try {
      final value = await block(TriResult<E>._());
      return value;
    } on _ResErr<E> catch (e) {
      return Err(e.err);
    } catch (e) {
      print("Result<$V, $E>#block() caught exception not of type $E; Please throw an instance of $E instead.");
      rethrow;
    }
  }
}

class Ok<V, E> extends Result<V, E> {
  final V value;

  const Ok(this.value);

  @override
  Result<V, E> and(Result<V, E> other) {
    return other;
  }

  @override
  Result<U, E> andThen<U>(Result<U, E> Function(V val) f) {
    return f(value);
  }

  @override
  Result<C, E> cast<C>() {
    return Ok(value as C);
  }

  @override
  Result<V, C> castErr<C>() {
    return Ok(value);
  }

  @override
  Option<E> err() {
    return None();
  }

  @override
  V expect(String message) {
    return value;
  }

  @override
  E expectErr(String message) {
    throw StateError("Expected Err, got Ok($value): $message");
  }

  @override
  Result<V, E> filterOr(bool Function(V val) f, E err) {
    return f(value) ? this : Err(err);
  }

  @override
  Result<V, E> filterOrElse(bool Function(V val) f, E Function(V val) err) {
    return f(value) ? this : Err(err(value));
  }

  @override
  Result<V, E> inspectErr(void Function(E err) f) {
    return this;
  }

  @override
  Result<V, E> inspectOk(void Function(V val) f) {
    f(value);
    return this;
  }

  @override
  bool get isErr => false;

  @override
  bool get isOk => true;

  @override
  Result<T, E> map<T>(T Function(V val) f) {
    return Ok(f(value));
  }

  @override
  Result<V, T> mapErr<T>(T Function(E err) f) {
    return Ok(value);
  }

  @override
  Option<V> ok() {
    return Some(value);
  }

  @override
  Result<V, E> or(Result<V, E> other) {
    return this;
  }

  @override
  Result<V, C> tryCastErrOr<C>(C err) {
    return Ok(value);
  }

  @override
  Result<V, C> tryCastErrOrElse<C>(C Function(E err) err) {
    return Ok(value);
  }

  @override
  Result<C, E> tryCastOr<C>(E err) {
    return Ok(value as C);
  }

  @override
  Result<C, E> tryCastOrElse<C>(E Function(V val) err) {
    return Ok(value as C);
  }

  @override
  V unwrap() {
    return value;
  }

  @override
  E unwrapErr() {
    throw StateError("Called unwrapErr() on Ok($value)");
  }

  @override
  E unwrapErrOr(E defaultValue) {
    return defaultValue;
  }

  @override
  E unwrapErrOrElse(E Function(V val) defaultValue) {
    return defaultValue(value);
  }

  @override
  V unwrapOr(V defaultValue) {
    return value;
  }

  @override
  V unwrapOrElse(V Function(E err) defaultValue) {
    return value;
  }

  @override
  Result<Pair<V, O>, E> zip<O>(Result<O, E> other) {
    return switch (other) {
      Ok(value: var a) => Ok(Pair(value, a)),
      Err(error: var a) => Err(a),
    };
  }

  @override
  Result<Pair<V, O>, E> zipWith<O>(Result<O, E> Function(V val) other) {
    return switch (other(value)) {
      Ok(value: var a) => Ok(Pair(value, a)),
      Err(error: var a) => Err(a),
    };
  }

  @override
  String toString() {
    return "Ok($value)";
  }

  @override
  bool operator ==(Object other) {
    return other is Ok<V, E> && other.value == value;
  }

  @override
  int get hashCode => value.hashCode;
}

class Err<V, E> extends Result<V, E> {
  final E error;

  const Err(this.error);

  @override
  Result<V, E> and(Result<V, E> other) {
    return this;
  }

  @override
  Result<U, E> andThen<U>(Result<U, E> Function(V val) f) {
    return Err(error);
  }

  @override
  Result<C, E> cast<C>() {
    return Err(error);
  }

  @override
  Result<V, C> castErr<C>() {
    return Err(error as C);
  }

  @override
  Option<E> err() {
    return Some(error);
  }

  @override
  V expect(String message) {
    throw StateError("Expected Ok, got Err($error): $message");
  }

  @override
  E expectErr(String message) {
    return error;
  }

  @override
  Result<V, E> filterOr(bool Function(V val) f, E err) {
    return Err(error);
  }

  @override
  Result<V, E> filterOrElse(bool Function(V val) f, E Function(V val) err) {
    return Err(error);
  }

  @override
  Result<V, E> inspectErr(void Function(E err) f) {
    f(error);
    return this;
  }

  @override
  Result<V, E> inspectOk(void Function(V val) f) {
    return this;
  }

  @override
  bool get isErr => true;

  @override
  bool get isOk => false;

  @override
  Result<T, E> map<T>(T Function(V val) f) {
    return Err(error);
  }

  @override
  Result<V, T> mapErr<T>(T Function(E err) f) {
    return Err(f(error));
  }

  @override
  Option<V> ok() {
    return None();
  }

  @override
  Result<V, E> or(Result<V, E> other) {
    return other;
  }

  @override
  Result<V, C> tryCastErrOr<C>(C err) {
    if (error is C) return Err(error as C);
    return Err(err);
  }

  @override
  Result<V, C> tryCastErrOrElse<C>(C Function(E err) err) {
    if (error is C) return Err(error as C);
    return Err(err(error));
  }

  @override
  Result<C, E> tryCastOr<C>(E err) {
    return Err(err);
  }

  @override
  Result<C, E> tryCastOrElse<C>(E Function(V val) err) {
    return Err(error);
  }

  @override
  V unwrap() {
    throw StateError("Called unwrap() on Err($error)");
  }

  @override
  E unwrapErr() {
    return error;
  }

  @override
  E unwrapErrOr(E defaultValue) {
    return error;
  }

  @override
  E unwrapErrOrElse(E Function(V val) defaultValue) {
    return error;
  }

  @override
  V unwrapOr(V defaultValue) {
    return defaultValue;
  }

  @override
  V unwrapOrElse(V Function(E err) defaultValue) {
    return defaultValue(error);
  }

  @override
  Result<Pair<V, O>, E> zip<O>(Result<O, E> other) {
    return Err(error);
  }

  @override
  Result<Pair<V, O>, E> zipWith<O>(Result<O, E> Function(V val) other) {
    return Err(error);
  }

  @override
  String toString() {
    return 'Err($error)';
  }

  @override
  bool operator ==(Object other) => other is Err<V, E> && other.error == error;

  @override
  int get hashCode => error.hashCode;
}

/// A function that combines the errors of multiple results.
///
/// ```dart
/// Result<int, ArgumentError> fibonacci(int n) {
///   if (n < 0) return Result.err(ArgumentError("n must be >= 0"));
///   if (n == 0 || n == 1) return Result.ok(n);
///   return Result.block((final tri) {
///     final a = fibonacci(n - 1)(tri);
///     final b = fibonacci(n - 2)(tri);
///     return Result.ok(a + b);
///   });
/// }
/// ```
class TriResult<E> {
  const TriResult._();

  T call<T>(Result<T, E> res) => switch (res) {
        Ok(value: var v) => v,
        Err(error: var e) => throw _ResErr(e),
      };

  T opt<T>(Option<T> opt, E err) => call(opt.okOr(err));

  T optOr<T>(Option<T> opt, E Function() err) => call(opt.okOrElse(err));
}

class _ResErr<E> {
  final E err;

  _ResErr(this.err);
}

extension ResultExtractFuture<V, E> on Result<Future<V>, E> {
  Future<Result<V, E>> extractFuture() {
    if (isOk)
      return unwrap().then(Result.ok);
    else
      return Future.value(this as Result<V, E>);
  }
}

extension FutureResult<V, E> on Future<Result<V, E>> {
  Future<V> unwrap() => then((res) => res.unwrap());

  Future<V> unwrapOr(V defaultValue) => then((res) => res.unwrapOr(defaultValue));

  Future<V> unwrapOrElse(V Function(E) defaultValue) => then((res) => res.unwrapOrElse(defaultValue));

  Future<E> unwrapErr() => then((res) => res.unwrapErr());

  Future<E> unwrapErrOr(E defaultValue) => then((res) => res.unwrapErrOr(defaultValue));

  Future<E> unwrapErrOrElse(E Function(V) defaultValue) => then((res) => res.unwrapErrOrElse(defaultValue));

  Future<bool> get isOk => then((res) => res.isOk);

  Future<bool> get isErr => then((res) => res.isErr);

  Future<Result<T, E>> map<T>(T Function(V) f) => then((res) => res.map(f));

  Future<Result<V, T>> mapErr<T>(T Function(E) f) => then((res) => res.mapErr(f));

  Future<Result<V, E>> andTimeoutOr(Duration timeLimit, E err) => timeout(timeLimit, onTimeout: () => Result.err(err));

  Future<Result<V, E>> andTimeoutOrElse(Duration timeLimit, E Function() factory) =>
      timeout(timeLimit, onTimeout: () => Result.err(factory()));

  Future<Result<V, E>> and(Result<V, E> other) => then((res) => res.and(other));

  Future<Result<V, E>> or(Result<V, E> other) => then((res) => res.or(other));

  Future<Result<V, E>> andThen(Result<V, E> Function(V) f) => then((res) => res.andThen(f));

  Future<Option<V>> ok() => then((res) => res.ok());

  Future<Option<E>> err() => then((res) => res.err());

  Future<V> call(TriResult<E> e) => then((res) => res.call(e));

  Future<V> tri(TriResult<E> e) => then((res) => res.tri(e));
}

extension ResultTimeoutX<T> on Future<T> {
  Future<Result<T, E>> timeoutOr<E>(Duration timeLimit, E err) =>
      then(Result<T, E>.ok).timeout(timeLimit, onTimeout: () => Result.err(err));

  Future<Result<T, E>> timeoutOrElse<E>(Duration timeLimit, E Function() factory) =>
      then(Result<T, E>.ok).timeout(timeLimit, onTimeout: () => Result.err(factory()));
}
