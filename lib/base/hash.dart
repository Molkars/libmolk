import 'package:libmolk_dart/base/cmp.dart';
import 'package:libmolk_dart/iter/iter.dart';

mixin Hash<T> on Eq<T> {
  int hash(T value);

  /// Returns 0 if unknown
  int maxCapacity() => -1;
}

class DefaultHash<T> with Eq<T>, Hash<T> {
  final int capacity;

  const DefaultHash([this.capacity = 1000]);

  @override
  int hash(T value) => value.hashCode;

  @override
  bool eq(T a, T b) => a == b;

  @override
  bool ne(T a, T b) => a != b;

  @override
  int maxCapacity() => capacity;
}

class StrHash with Eq<String>, Hash<String> {
  @override
  int hash(String value) {
    int byte, crc, mask;

    crc = 0xFFFFFFFF;
    final bytes = value.codeUnits;
    for (var i = 0; i < bytes.length; i++) {
      byte = bytes[i]; // Get next byte.
      crc = crc ^ byte;
      for (int j = 0; j < 8; j++) {
        mask = -(crc & 1);
        crc = (crc >> 1) ^ (0xEDB88320 & mask);
      }
      i = i + 1;
    }
    return ~crc;
  }

  @override
  bool eq(String a, String b) {
    return a
        .chars()
        .zip(b.chars())
        .every((a, b) => a == b);
  }

  @override
  bool ne(String a, String b) {
    return !eq(a, b);
  }
}

class NumHash with Eq<int>, Hash<int> {
  @override
  int hash(int value) => value;

  @override
  bool eq(int a, int b) => a == b;

  @override
  bool ne(int a, int b) => a != b;
}