import 'package:libmolk_dart/libmolk_dart.dart';

class BTreeMap<K, V, C extends Cmp<K>> extends MMap<K, V> {
  _Node<K, V>? _root;
  final C _cmp;

  BTreeMap(this._cmp);

  @override
  bool containsKey(K key) => get(key).isSome;

  @override
  Option<V> get(K key) {
    var node = _root;

    while (node != null) {
      var cmp = _cmp.cmp(key, node.key);
      if (cmp == 0) return Option.some(node.value);

      if (cmp < 0) {
        node = node.left;
      } else {
        node = node.right;
      }
    }

    return Option.none();
  }

  @override
  Option<V> insert(K key, V value) {
    if (_root == null) {
      _root = _Node(key, value);
      return Option.none();
    }

    var node = _root;
    while (node != null) {
      var cmp = _cmp.cmp(key, node.key);
      if (cmp == 0) {
        var oldValue = node.value;
        node.value = value;
        return Option.some(oldValue);
      }

      if (cmp < 0) {
        node = node.left;
      } else {
        node = node.right;
      }
    }

    return Option.none();
  }

  @override
  Option<V> remove(K key) {
    if (_root == null) return Option.none();
    var root = _root!;

    if (_cmp.cmp(key, root.key) == 0) {
      final out = root.value;

      if (root.right == null) {
        _root = null;
        return Option.some(out);
      }

      var node = root.right!;
      while (node.left != null) {
        node = node.left!;
      }
      node.left = root.left;
      _root = node;

      return Option.some(out);
    }

    var parent = root;
    do {
      final cmp = _cmp.cmp(key, parent.key);
      assert(cmp != 0, 'Your comparison function is not working properly!');

      if (cmp < 0) {
        if (parent.left == null) return Option.none();

        if (_cmp.cmp(key, parent.left!.key) != 0) {
          parent = parent.left!;
          continue;
        }

        final out = parent.left!.value;
        if (parent.left!.right == null) {
          parent.left = null;
          return Option.some(out);
        }

        var node = parent.left!.right!;
        while (node.left != null) {
          node = node.left!;
        }
        node.left = parent.left!.left;
        parent.left = node;

        return Option.some(out);
      }

      if (parent.right == null) return Option.none();

      if (_cmp.cmp(key, parent.right!.key) != 0) {
        parent = parent.right!;
        continue;
      }

      final out = parent.right!.value;
      if (parent.right!.left == null) {
        parent.right = null;
        return Option.some(out);
      }

      var node = parent.right!.left!;
      while (node.right != null) {
        node = node.right!;
      }
      node.right = parent.right!.right;
      parent.right = node;
    } while (true);
  }

  @override
  Entry<K, V> entry(K key) {
    if (_root == null) {
      return _RootEntry(key, this);
    }

    var node = _root;
    do {
      var cmp = _cmp.cmp(key, node!.key);
      if (cmp == 0) {
        return _OccupiedEntry._(key, node);
      }
      _Node<K, V>? child;
      Side side;
      if (cmp < 0) {
        child = node.right;
        side = Side.left;
      } else {
        child = node.left;
        side = Side.right;
      }

      if (child == null) {
        return _EmptyEntry._(key, side, node);
      }
    } while (true);
  }

  @override
  bool get isEmpty => _root == null;

  @override
  bool get isNotEmpty => _root != null;

  @override
  PairIter<K, V> iter() => _InOrderIter<K, V>(_root);
}

class _Node<K, V> {
  final K key;
  V value;

  _Node<K, V>? left;
  _Node<K, V>? right;

  _Node(this.key, this.value);
}

enum Side { left, right }

class _EmptyEntry<K, V> extends VacantEntry<K, V> {
  final K key;
  final Side _side;
  final _Node<K, V> _node;

  _EmptyEntry._(this.key, this._side, this._node);

  @override
  void fill(V value) {
    if (_side == Side.left) {
      _node.left = _Node<K, V>(key, value);
    } else {
      _node.right = _Node<K, V>(key, value);
    }
  }

  @override
  void insert(V value) => fill(value);
}

class _RootEntry<K, V, C extends Cmp<K>> extends VacantEntry<K, V> {
  final K key;
  final BTreeMap<K, V, C> _map;

  _RootEntry(this.key, this._map);

  @override
  void fill(V value) {
    if (_map.isEmpty) {
      _map._root = _Node<K, V>(key, value);
    } else {
      // If the map has been grown since this entry was created,
      // we need to find the correct place to insert the new key.
      _map.entry(key).insert(value);
    }
  }
}

class _OccupiedEntry<K, V> extends OccupiedEntry<K, V> {
  final K key;
  final _Node<K, V> _node;

  _OccupiedEntry._(this.key, this._node);

  @override
  V replace(V value) {
    var out = _node.value;
    _node.value = value;
    return out;
  }

  @override
  void insert(V value) => replace(value);
}

class _InOrderIter<K, V> with PairIter<K, V> {
  final _queue = MLinkedList<_Node<K, V>>();

  _InOrderIter(_Node<K, V>? root) {
    _insert(root);
  }

  void _insert(_Node<K, V>? node) {
    while (node != null) {
      _queue.prepend(node);
      node = node.left;
    }
  }

  @override
  Option<Pair<K, V>> next() {
    return _queue.popBack().map((v) {
      if (v.right != null) _insert(v.right);

      return Pair(v.key, v.value);
    });
  }
}
