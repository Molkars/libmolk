import 'package:libmolk_dart/base/option.dart';
import 'package:libmolk_dart/iter/pair_iter.dart';

abstract class MMap<K, V> {
  Entry<K, V> entry(K key);

  Option<V> get(K key);
  Option<V> insert(K key, V value);
  Option<V> remove(K key);

  bool get isEmpty;
  bool get isNotEmpty;

  bool containsKey(K key) => get(key).isSome;

  PairIter<K, V> iter();
}

abstract class Entry<K, V> {
  void insert(V value);
}

abstract class VacantEntry<K, V> extends Entry<K, V> {
  void fill(V value);

  @override
  void insert(V value) => fill(value);
}

abstract class OccupiedEntry<K, V> extends Entry<K, V> {
  V replace(V value);

  @override
  void insert(V value) => replace(value);
}
