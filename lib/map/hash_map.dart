import 'dart:math';

import 'package:libmolk_dart/base/hash.dart';
import 'package:libmolk_dart/base/option.dart';
import 'package:libmolk_dart/base/pair.dart';
import 'package:libmolk_dart/iter/iter.dart';
import 'package:libmolk_dart/iter/pair_iter.dart';
import 'package:libmolk_dart/list/linked_list.dart';
import 'package:libmolk_dart/list/vector.dart';
import 'package:libmolk_dart/map/map.dart';

class _Entry<K, V> implements Pair<K, V> {
  @override
  final K key;

  @override
  V value;

  _Entry(this.key, this.value);

  @override
  K get left => key;

  @override
  V get right => value;
}

class MHashMap<K, V> extends MMap<K, V> {
  final Hash<K> _hash;
  final MVector<MLinkedList<_Entry<K, V>>?> _data;
  int _length = 0;
  final int _cap;

  MHashMap([Hash<K>? hash])
      : _hash = hash ?? DefaultHash(),
        _data = MVector.withCapacity(max(1, hash?.maxCapacity() ?? 1000)),
        _cap = max(1, hash?.maxCapacity() ?? 1000);

  void clear() {
    _data.clear();
  }

  @override
  bool containsKey(K key) => get(key).isSome;

  @override
  Entry<K, V> entry(K key) {
    final hash = _hash.hash(key) % _cap;
    if (_data[hash] == null) return _VEntry(this, key, hash);
    return _OEntry(this, key, hash);
  }

  @override
  Option<V> get(K key) {
    final index = _hash.hash(key) % _cap;
    if (index >= _data.length()) {
      return Option.none();
    }
    final list = _data[index];
    if (list == null) {
      return Option.none();
    }
    return list.iter().find((e) => _hash.eq(key, e.key)).map((e) => e.value);
  }

  @override
  bool get isEmpty => _length == 0;

  @override
  bool get isNotEmpty => _length > 0;

  @override
  Option<V> remove(K key) {
    final index = _hash.hash(key) % _cap;
    if (index >= _data.length()) return Option.none();
    final list = _data[index];
    if (list == null) return Option.none();

    final idx = list.iter().position((v) => _hash.eq(key, v.key));
    return idx.andThen((idx) => list.iter().nth(idx).map((v) => v.value));
  }

  @override
  Option<V> insert(K key, V value) {
    final index = _hash.hash(key) % _cap;
    if (index >= _data.length()) {
      _data.grow(index + 1);
      _data.fill(_data.length(), _data.capacity(), null);
    }
    final list = _data[index] ??= MLinkedList<_Entry<K, V>>();
    final opt = list.iter().find((e) => _hash.eq(key, e.key));
    return opt.map((pair) {
      final old = pair.value;
      pair.value = value;
      return old;
    }).inspectNone(() {
      list.push(_Entry(key, value));
      _length++;
    });
  }

  @override
  PairIter<K, V> iter() {
    return _HashMapIter(this);
  }
}

class _HashMapIter<K, V> with PairIter<K, V> {
  final MHashMap<K, V> _map;
  int _index = 0;
  Iter<Pair<K, V>>? _iter;

  _HashMapIter(this._map);

  @override
  Option<Pair<K, V>> next() {
    return (_iter?.next() ?? Option.none()).orElse(() {
      _map._data.get(_index).filterNotNull().inspectSome((val) {
        _iter = val.iter();
        _index++;
      });
      return next();
    });
  }
}

class _OEntry<K, V> extends OccupiedEntry<K, V> {
  final MHashMap<K, V> map;
  final K key;
  final int hash;

  _OEntry(this.map, this.key, this.hash);

  @override
  V replace(V value) {
    final list =
        map._data.get(hash).filterNotNull().expect("Missing entry for $key");
    final entry = list
        .iter()
        .find((val) => val.key == key)
        .expect("Missing entry for $key");
    final old = entry.value;
    entry.value = value;
    return old;
  }
}

class _VEntry<K, V> extends VacantEntry<K, V> {
  final MHashMap<K, V> map;
  final K key;
  final int hash;

  _VEntry(this.map, this.key, this.hash);

  @override
  void fill(V value) {
    if (hash >= map._data.length()) {
      map._data.grow(hash + 1);
      map._data.fill(map._data.length(), map._data.capacity(), null);
    }
    final list = map._data[hash] ??= MLinkedList<_Entry<K, V>>();
    list
        .iter()
        .find((e) => map._hash.eq(key, e.key))
        .inspectNone(() => list.push(_Entry(key, value)));
  }
}
