import 'package:libmolk_dart/base/option.dart';
import 'package:libmolk_dart/iter/iter.dart';

abstract class MList<T> with IntoIter<T> {
  const MList();

  int length();

  void push(T val);

  bool remove(T val);

  Option<T> pop();
}

abstract class MIndexedList<T> implements MList<T> {
  const MIndexedList();

  T operator [](int index);

  void insert(int index, T value);

  Option<T> removeAt(int index);

  Option<T> get(int index);
}
