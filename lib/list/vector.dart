import 'dart:math';

import 'package:libmolk_dart/base/option.dart';
import 'package:libmolk_dart/iter/iter.dart';
import 'package:libmolk_dart/list/list.dart';

class MVector<T> extends MIndexedList<T> {
  List<T?> _list;
  int _length;

  MVector.from(List<T> list)
      : _list = List.from(list, growable: true),
        _length = list.length;

  MVector.withCapacity(int capacity)
      : _list = List.filled(capacity, null, growable: true),
        _length = 0;

  MVector() : this.from([]);

  @override
  int length() => _length;

  int capacity() => _list.length;

  bool get isEmpty => _length == 0;

  bool get isNotEmpty => _length != 0;

  @override
  Option<T> pop() {
    if (isEmpty) {
      return Option.none();
    }
    var out = _list[_length - 1];
    _length--;
    return Option.some(out as T);
  }

  @override
  void push(T val) {
    _list.add(val);
    _length++;
  }

  @override
  bool remove(T val) {
    for (int i = 0; i < _list.length; i++) {
      if (_list[i] == val) {
        _list.removeAt(i);
        _length--;
        return true;
      }
    }
    return false;
  }

  void grow(int length) {
    if (length <= _list.length) {
      return;
    }
    for (int i = _list.length; i < length; i++) {
      _list.add(null);
    }
  }

  void fill(int start, int end, T val) {
    if (start > end) throw ArgumentError('MVector.fill: start <= end was false');
    if (start >= capacity()) throw RangeError.range(start, 0, max(0, capacity() - 1));
    if (end > capacity()) throw RangeError.range(end, 1, max(0, capacity()));
    for (int i = start; i < end; i++) {
      _list[i] = val;
    }
    _length = end;
  }

  void operator []=(int index, T val) {
    if (index >= capacity()) throw RangeError.range(index, 0, _length);
    if (index >= _length) _length = index + 1;
    _list[index] = val;
  }

  @override
  T operator [](int index) => _list[index] as T;

  @override
  Option<T> get(int index) =>
      index < _list.length ? Option.some(_list[index] as T) : Option.none();

  @override
  void insert(int index, T value) => _list.insert(index, value);

  @override
  Option<T> removeAt(int index) => index < _list.length ? Option.some(_list.removeAt(index) as T) : Option.none();

  @override
  Iter<T> iter() => _Iter(_list, _length);

  Option<T> front() => get(0);
  Option<T> back() => get(_length - 1);

  void clear() {
    _length = 0;
    _list = [];
  }
}

class _Iter<T> with Iter<T> {
  final List<T?> _list;
  final int _length;
  int _index;

  _Iter(this._list, this._length) : _index = 0;

  @override
  Option<T> next() {
    if (_index < _length) {
      return Option.some(_list[_index++] as T);
    }
    return Option.none();
  }
}
