import 'package:libmolk_dart/base/option.dart';
import 'package:libmolk_dart/iter/iter.dart';
import 'package:libmolk_dart/list/list.dart';

class MLinkedList<T> extends MList<T> {
  _Node<T>? _front;
  _Node<T>? _back;
  int _length = 0;

  MLinkedList();

  MLinkedList.from(List<T> values) {
    values.forEach(append);
  }

  @override
  int length() => _length;

  bool get isEmpty => _length == 0;
  bool get isNotEmpty => _length != 0;

  @override
  void push(T val) => append(val);

  @override
  Option<T> pop() => popBack();

  @override
  Iter<T> iter() => _Iter(_front);

  Iter<T> revIter() => _RevIter(_back);

  Option<T> front() {
    if (_front == null) {
      return Option.none();
    } else {
      return Option.some(_front!.value);
    }
  }

  Option<T> popFront() => front().inspectSome((_) {
    _front = _front?.next;
    if (_front == null) _back = null;
    _length--;
  });

  Option<T> back() {
    if (_back == null) {
      return Option.none();
    } else {
      return Option.some(_back!.value);
    }
  }

  Option<T> popBack() => back().inspectSome((_) {
    _back = _back?.prev;
    if (_back == null) _front = null;
    _length--;
  });

  static Collector<T, MLinkedList<T>> collector<T>() => MLinkedListCollector();

  void append(T value) {
    _length++;
    if (_back == null) {
      _front = _back = _Node.first(value);
      return;
    }

    _back = _Node.back(value, _back!);
  }

  void prepend(T value) {
    _length++;
    if (_front == null) {
      _front = _back = _Node.first(value);
      return;
    }

    _front = _Node.front(value, _front!);
  }

  @override
  String toString() {
    final sb = StringBuffer();
    sb.write('[');
    for (var node = _front; node != null; node = node.next) {
      sb.write('${node.value}');
      if (node.next != null) {
        sb.write(', ');
      }
    }
    sb.write(']');
    return sb.toString();
  }

  @override
  bool remove(T val) {
    for (_Node<T>? node = _front; node != null; node = node.next) {
      if (node.value == val) {
        node.next?.prev = node.prev;
        node.prev?.next = node.next;
        if (node == _front) {
          _front = node.next;
        }
        if (node == _back) {
          _back = node.prev;
        }
        _length--;
        return true;
      }
    }
    return false;
  }
}

class _Node<T> {
  T value;
  _Node<T>? next;
  _Node<T>? prev;

  _Node.first(this.value);

  _Node.front(this.value, _Node<T> this.next) {
    next!.prev = this;
  }

  _Node.back(this.value, _Node<T> this.prev) {
    prev!.next = this;
  }

  @override
  String toString() {
    return '_Node{value: $value, next: $next}';
  }
}

class _Iter<T> with Iter<T> {
  _Node<T>? _node;

  _Iter(this._node);

  @override
  Option<T> next() {
    if (_node == null) {
      return Option.none();
    }

    var out = _node!.value;
    _node = _node!.next;
    return Option.some(out);
  }
}

class _RevIter<T> with Iter<T> {
  _Node<T>? _node;

  _RevIter(this._node);

  @override
  Option<T> next() {
    if (_node == null) {
      return Option.none();
    }

    var out = _node!.value;
    _node = _node!.prev;
    return Option.some(out);
  }
}

class MLinkedListCollector<T> with Collector<T, MLinkedList<T>> {
  @override
  MLinkedList<T> collect(Iter<T> iter) =>
      iter.fold(MLinkedList(), (list, value) => list..append(value));
}
