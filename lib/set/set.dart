
import 'package:libmolk_dart/iter/iter.dart';

abstract class MSet<T> {
  bool contains(T value);
  bool insert(T value);
  bool remove(T value);

  int count();

  MSet();
  MSet.from(Iterable<T> values) {
    values.iter().forEach(insert).ignore();
  }
}