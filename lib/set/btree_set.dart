import 'package:libmolk_dart/base/cmp.dart';
import 'package:libmolk_dart/base/ptr.dart';
import 'package:libmolk_dart/base/option.dart';
import 'package:libmolk_dart/iter/iter.dart';
import 'package:libmolk_dart/set/set.dart';

import '../list/linked_list.dart';

class _Node<T> {
  Ptr<_Node<T>> left = Ptr.none();
  Ptr<_Node<T>> right = Ptr.none();
  T value;

  _Node(this.value);

  @override
  String toString() {
    final sb = StringBuffer();
    sb.write('(');
    if (left.isSome) sb.write('${left.deref()} <- ');
    sb.write('$value');
    if (right.isSome) sb.write(' -> ${right.deref()}');
    sb.write(')');

    return sb.toString();
  }
}

class MBTreeSet<T> extends MSet<T> {
  Ptr<_Node<T>> _root = Ptr.none();
  Cmp<T> _cmp;
  int _count = 0;

  MBTreeSet([Cmp<T>? cmp]) : _cmp = cmp ?? DefaultCmp();

  MBTreeSet.from(Iterable<T> iterable, [Cmp<T>? cmp])
      : _cmp = cmp ?? DefaultCmp() {
    iterable.iter().forEach(insert).ignore();
  }

  int count() => _count;

  @override
  bool contains(T value) {
    for (var node = _root; node.isSome; node = node.deref().left) {
      if (_cmp.cmp(node.deref().value, value) == 0) return true;
    }
    return false;
  }

  @override
  bool insert(T value) {
    if (_root.isNone) {
      _root = Ptr(_Node(value));
      assert(_count == 0);
      _count = 1;
      return true;
    }

    var node = _root.deref();
    do {
      var cmp = _cmp.cmp(node.value, value);
      if (cmp == 0) return false;

      if (cmp < 0) {
        final child = node.left;
        if (child.isNone) {
          node.left = Ptr(_Node(value));
          _count++;
          return true;
        }
        node = child.deref();
      } else {
        final child = node.right;
        if (child.isNone) {
          node.right = Ptr(_Node(value));
          _count++;
          return true;
        }
        node = child.deref();
      }
    } while (true);
  }

  @override
  bool remove(T value) {
    if (_root.isNone) return false;

    final root = _root.deref();
    if (_cmp.cmp(root.value, value) == 0) {
      if (root.right.isNone) {
        _root = root.left;
        return true;
      }

      var node = root.right.deref();
      while (node.left.isSome) {
        node = node.left.deref();
      }

      node.left = root.left;
      _root = root.right;
      return true;
    }

    var parent = _root.deref();
    do {
      final cmp = _cmp.cmp(parent.value, value);
      assert(cmp != 0);

      if (cmp < 0) {
        if (parent.left.isNone) return false; // Not found

        final child = parent.left.deref();

        if (_cmp.cmp(child.value, value) != 0) {
          parent = child;
          continue;
        }

        if (child.right.isNone) {
          parent.left = child.left;
          return true;
        }

        var node = child.right.deref();
        while (node.left.isSome) {
          node = node.left.deref();
        }

        node.left = child.left;
        parent.left = child.right;
        return true;
      }

      if (parent.right.isNone) return false;

      final child = parent.right.deref();
      if (_cmp.cmp(child.value, value) != 0) {
        parent = child;
        continue;
      }

      if (child.left.isNone) {
        parent.right = child.right;
        return true;
      }

      var node = child.left.deref();
      while (node.right.isSome) {
        node = node.right.deref();
      }

      node.right = child.right;
      parent.right = child.left;
      return true;
    } while (true);
  }

  @override
  String toString() {
    final buffer = StringBuffer();
    buffer.write('MBTreeSet {');

    final queue = MLinkedList<Ptr<_Node<T>>>();

    queue.push(_root);
    while (queue.isNotEmpty) {
      final top = queue.pop().unwrap();
      if (top.isNone) continue;
      final val = top.deref();
      buffer.write(' ${val.value}');

      if (val.left.isSome) queue.push(val.left);
      if (val.right.isSome) queue.push(val.right);

      if (queue.isNotEmpty) buffer.write(',');
    }

    buffer.write(' }');
    return buffer.toString();
  }

  Iter<T> inOrder() => _InOrder(_root);
  Iter<T> postOrder() => _PostOrder(_root);
  Iter<T> preOrder() => _PreOrder(_root);
}

class _InOrder<T> with Iter<T> {
  final MLinkedList<_Node<T>> _stack;

  _InOrder(Ptr<_Node<T>> root) : _stack = MLinkedList() {
    _moveRight(root);
  }

  void _moveRight(Ptr<_Node<T>> current) {
    while (current.isSome) {
      final node = current.deref();
      _stack.push(node);
      current = node.right;
    }
  }

  @override
  Option<T> next() {
    return _stack.popBack().map((node) {
      if (node.left.isSome) _moveRight(node.left);

      return node.value;
    });
  }
}

class _PreOrder<T> with Iter<T> {
  final MLinkedList<_Node<T>> _stack;

  _PreOrder(Ptr<_Node<T>> root): _stack = MLinkedList() {
    _stack.push(root.deref());
  }

  @override
  Option<T> next() {
    return _stack.popBack().map((node) {
      if (node.left.isSome) _stack.push(node.left.deref());
      if (node.right.isSome) _stack.push(node.right.deref());
      return node.value;
    });
  }
}

class _PostOrder<T> with Iter<T> {
  final MLinkedList<_Node<T>> _stack;

  _PostOrder(Ptr<_Node<T>> root): _stack = MLinkedList() {
    _push(root);
  }

  void _push(Ptr<_Node<T>> current) {
    while (current.isSome) {
      final node = current.deref();
      _stack.push(node);
      current = node.right.isSome ? node.right : node.left;
    }
  }

  @override
  Option<T> next() {
    return _stack.popBack().map((node) {
      _stack
          .back()
          .filter((v) => v.left.isSome && v.right.deref() == node)
          .inspectSome((v) => _push(v.left));
      return node.value;
    });
  }
}