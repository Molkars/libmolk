import 'package:libmolk_dart/base/hash.dart';
import 'package:libmolk_dart/base/option.dart';
import 'package:libmolk_dart/iter/iter.dart';
import 'package:libmolk_dart/list/vector.dart';
import 'package:libmolk_dart/set/set.dart';

import '../list/linked_list.dart';

class MHashSet<T> extends MSet<T> {
  final MVector<MLinkedList<T>?> _data;
  final Hash<T> _hash;
  int _count = 0;

  MHashSet([Hash<T>? hash])
      : _hash = hash ?? DefaultHash(),
        _data = MVector.withCapacity(hash?.maxCapacity() ?? 0);

  MHashSet.from(Iterable<T> values, [Hash<T>? hash])
      : _hash = hash ?? DefaultHash(),
        _data = MVector.withCapacity(hash?.maxCapacity() ?? 0),
        super.from(values);

  /// Returns false if [element] was present in the set.
  @override
  bool insert(T value) {
    final index = _hash.hash(value);
    if (_data
        .get(index)
        .filterNotNull()
        .filter((list) => list.iter().contains(value))
        .isSome) {
      return false;
    }

    if (index >= _data.capacity()) {
      _data.grow(index + 1);
      _data.fill(_data.length(), _data.capacity(), null);
    }

    (_data[index] ??= MLinkedList()).push(value);
    _count++;

    return true;
  }

  @override
  int count() => _count;

  @override
  bool contains(T value) {
    final index = _hash.hash(value);
    return index < _data.length() && _data[index].iterOrNone().contains(value);
  }

  @override
  bool remove(T value) {
    final index = _hash.hash(value);
    if (index >= _data.length()) {
      return false;
    }
    return _data[index]?.remove(value) ?? false;
  }
}